({
    openBucket : function (component, event, helper) {
        component.set("v.openBucket", true);
    },
    
    closeLookupWindow : function(component, event, heler){
        document.getElementById('listbox-id-1').style.display = 'None';
    },
    
    closeBucket : function (component, event, helper) {
        component.set("v.openBucket", false);
    },
    
    closeDuplicatesPopup : function (component, event, helper) {
        component.set("v.duplicatesFound", false);
    },
    
    doInit: function (component, event, helper) {
       helper.init(component, event, helper);
    },
    
    submitPeriodRequest : function(component, event, helper){
       
        var requestList = component.get("v.requestList");
        
        console.log(JSON.stringify(requestList));
        
        if(requestList.length > 0){
            var action = component.get('c.submitRequests');
            action.setParams({ listRequest : requestList});
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message: 'Your request has been placed successfully!!!',
                        type : 'success'
                    });
                    toastEvent.fire();
                    component.set("v.openBucket",false);
                    
                    var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": '00BZ0000001fKKiMAM',
                        "listViewName": 'Pending_Request',
                        "scope": "Period__c"
                    });
                    navEvent.fire();
                    
                    
                    helper.init(component, event, helper);
                } else if (state === "ERROR") {
                    
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            var resultsToast1 = $A.get("e.force:showToast");
                            resultsToast1.setParams({
                                "title": "ERROR",
                                "type":"error",
                                "message": "Error message: " + errors[0].message
                            });
                    		resultsToast1.fire();
                            component.set("v.openBucket",false);
                    		$A.get('e.force:refreshView').fire();
                        }
                    } else {
                        var resultsToast2 = $A.get("e.force:showToast");
                            resultsToast2.setParams({
                                "title": "ERROR",
                                "type":"error",
                                "message": "Unknown Error : Contact your system administrator."
                            });
                    		resultsToast2.fire();
                        	component.set("v.openBucket",false);
                    		$A.get('e.force:refreshView').fire();
                    }
                }
                else{
                    component.set("v.openBucket",false);
                    $A.get('e.force:refreshView').fire();
                }
            }));
            $A.enqueueAction(action);
        }
    },
    
    addRequest : function(component, event, helper){
        
        
        if(helper.validateForm(component,event)){
        
            component.set("v.showSpinner",true);
            var clients = component.get("v.clients");
            for(var i = 0 ; i < clients.length ; i++){
                if(typeof clients[i].WBS__r != 'undefined'){
                    for(var j = 0 ; j < clients[i].WBS__r.length ; j++){
                        if(typeof clients[i].WBS__r[j] != 'undefined' && clients[i].WBS__r[j].Id == component.get("v.period.WBS__c")){
                            component.set("v.period.WBS_TextValue__c",clients[i].WBS__r[j].Name);
                            component.set("v.period.Client_Text__c",clients[i].Name);
                            component.set("v.period.WBS__c",clients[i].WBS__r[j].Id);
                            component.set("v.period.Client_Name__c",clients[i].Id);
                            component.set("v.period.Approver_Name__c",component.get("v.directorName"));
                            component.set("v.period.Director__c ",component.get("v.directorLookupId"));  
                            break;
                        }  
                    }
                }
            } 
                  
            
            var action = component.get('c.checkDuplicatePeriods');
            
            console.log('Period -> ' + JSON.stringify(component.get("v.period")));
            
            action.setParams({ objPeriod : component.get("v.period")});           
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    var duplicates = response.getReturnValue();
                    
                    console.log('duplicates -> ' + duplicates.length);
                    
                    
                    if(duplicates.length == 0){
                        
                        var requestList = component.get("v.requestList");
                        if(requestList != null){
                            
                            requestList.push(component.get("v.period"));
                            component.set("v.requestList",requestList);
                            component.set("v.bucketSize",requestList.length);
                            component.set("v.period",{'sobjectType':'Period__c',
                                                      'RecordTypeId':'0126F000001Yp9BQAS',
                                                      'Status__c':'Pending Approval'});
                            component.set("v.clientName",null);
                            component.set("v.clientNameId",null);
                            component.set("v.wbslist",null);
                            component.set("v.directorName",null);
                            component.set("v.directorLookupId",null);
                        }
                        else{
                            var temp = [];
                            temp.push(component.get("v.period"));
                            component.set("v.requestList",temp);
                            component.set("v.bucketSize",temp.length);
                            component.set("v.period",{'sobjectType':'Period__c',
                                                      'RecordTypeId':'0126F000001Yp9BQAS',
                                                      'Status__c':'Pending Approval'});
                            component.set("v.clientName",null);
                            component.set("v.clientNameId",null);
                            component.set("v.wbslist",null);
                            component.set("v.directorName",null);
                            component.set("v.directorLookupId",null);
                        } 
                    }
                    else{
                        
                        for (var i = 0; i < duplicates.length; i++) {
                            duplicates.OwnerId = duplicates[i].Owner.Name;
                            duplicates.Director__c = duplicates[i].Director__r.Name;
                        }
                        
                        component.set("v.duplicatesFound",true);
                        component.set("v.duplicatePeriods",duplicates);
                    }
                    
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": "Something went wrong, plese check with your system administrator."
                    });
                    resultsToast1.fire();
                }
                else{}
            }));
            component.set("v.showSpinner",false);
            $A.enqueueAction(action);  
        }
    },
    
    refreshPage : function(component, event, helper){
        $A.get('e.force:refreshView').fire();
    },
    
    searchAccount : function(component, event, helper){
        
        component.set("v.clientNameId",'');
        
        component.set("v.searchClients",true);
        var src = event.target || event.srcElement;
        var searchText = event.target.value;
        if(searchText.length > 2){
            console.log(searchText.length);
            var action = component.get('c.searchClients');
            action.setParams({ searchText : searchText});           
            var cmpTarget = component.find('clientlookup');
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.clients", response.getReturnValue());
                    component.set("v.dataFound",true);
                    $A.util.addClass(cmpTarget, 'slds-is-open');
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": "Something went wrong, plese check with your system administrator."
                    });
                    $A.util.removeClass(cmpTarget, 'slds-is-open');
                    component.set("v.dataFound",false);
                    resultsToast1.fire();
                }
                    else{
                        $A.util.removeClass(cmpTarget, 'slds-is-open');
                        component.set("v.dataFound",false);
                    }
            }));
            $A.enqueueAction(action);
        }
    },
    
    selectAccount : function(component, event, helper){
        
        component.set("v.clientNameId",null);
        component.set("v.clientName",null);
        
        component.set("v.showSpinner",true);
        
        var src = event.target || event.srcElement;
        var selectedClient = event.target.id;
        var clients = component.get("v.clients");
        
        var array = [];
        
        for(var i = 0 ; i < clients.length ; i++){
            if(clients[i].Id === selectedClient){
                component.set("v.clientNameId",clients[i].Id);
                component.set("v.clientName",clients[i].Name);
   
                if(typeof clients[i].WBS__r != 'undefined'){
                    var obj1 = {};
                    obj1.label = '--None--';
                    obj1.value = null;
                    array.push(obj1); 
                    
                    for(var j = 0 ; j < clients[i].WBS__r.length ; j++){
                        var obj = {};
                        if(typeof clients[i].WBS__r[j] != 'undefined'){
                            obj.label = clients[i].WBS__r[j].Name;    
                            obj.value = clients[i].WBS__r[j].Id;
                        }
                        array.push(obj);  
                    } 
                }
                helper.validateClientLookup(component,event);
                break;
                component.set("v.clients", []);
            }
        }
        
        component.set("v.wbslist",array);
        var cmpTarget = component.find('clientlookup');
        $A.util.removeClass(cmpTarget, 'slds-is-open');
        component.set("v.showSpinner",false);
        component.set("v.searchClients",false);
        
    },
    
    searchApprovers : function(component, event, helper){
        
        component.set("v.directorLookupId",'');
        
        component.set("v.searchDirectors",true);
        var src = event.target || event.srcElement;
        var searchText = event.target.value;
        
        if(searchText.length > 2){
            var action = component.get('c.searchDirectors');
            action.setParams({ searchText : searchText});           
            var cmpTarget = component.find('approverlookup');
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log(JSON.stringify(response.getReturnValue()));
                    component.set("v.approvers", response.getReturnValue());
                    $A.util.addClass(cmpTarget, 'slds-is-open');
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": "Something went wrong, plese check with your system administrator."
                    });
                    $A.util.removeClass(cmpTarget, 'slds-is-open');
                    resultsToast1.fire();
                }
                    else{
                        $A.util.removeClass(cmpTarget, 'slds-is-open');
                    }
            }));
            $A.enqueueAction(action);
        }
    },
    
    selectApprover : function(component, event, helper){
        
        component.set("v.directorLookupId",null);
        component.set("v.directorName",null);
        
        var src = event.target || event.srcElement;
        var selectedClient = event.target.id;
        var approvers = component.get("v.approvers");
        
        for(var i = 0 ; i < approvers.length ; i++){
            if(approvers[i].Id === selectedClient){
                console.log('Test ' + approvers[i].Name);
                component.set("v.directorLookupId",approvers[i].Id);
                component.set("v.directorName",approvers[i].Name);
                component.set("v.searchDirectors",false);
                helper.validateApproverLookup(component,event);
                break;
            }
        }
        var cmpTarget = component.find('approverlookup');
        $A.util.removeClass(cmpTarget, 'slds-is-open');
        component.set("v.approvers", []);
    },
    
    handleRowAction: function (component, event, helper) {
        component.set("v.showSpinner",true);
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'show_details':
                console.log('Showing Details: ' + JSON.stringify(row));
                component.set("v.row",row);
                component.set("v.showRequestData",true);
                break;
            case 'delete':
                helper.removeRequest(component, row)
                break;
        }
        component.set("v.showSpinner",false);
    },
    
    handleRowActionDuplicate: function (component, event, helper) {
        component.set("v.showSpinner",true);
        var action = event.getParam('action');
        var row = event.getParam('row');
        
        console.log('Id ->' + JSON.stringify(row));

        switch (action.name) {
            case 'show_details':
                component.set("v.periodId",row.Id);
                component.set("v.showData",true);
                break;
        }
        component.set("v.showSpinner",false);
    },
    
    closeDataPopup : function (component, event, helper) {
        component.set("v.showData",false);
    },
    
    closeRequestDataPopup : function (component, event, helper) {
        component.set("v.showRequestData",false);
    },
    
    validateFormData : function(component, event, helper){
        helper.validateForm(component,event);
    }
    
    
})