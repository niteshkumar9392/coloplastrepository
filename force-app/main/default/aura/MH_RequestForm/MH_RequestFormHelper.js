({
    init: function (component, event, helper) {
        
        //component.set("v.showSpinner",true);
        var action = component.get('c.getPeriodPicklistValues');
        action.setCallback(this, $A.getCallback(function (response) {
            console.log('Data -> ' + response.getReturnValue());
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.periodPicklists", response.getReturnValue());
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
        
        //component.set("v.showSpinner",true);
        var getProfile = component.get('c.getUserProfile');
        getProfile.setCallback(this, $A.getCallback(function (response) {
            console.log('Profile -> ' + response.getReturnValue());
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userProfile", response.getReturnValue());
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(getProfile);
        
        
        var actionsDuplicate = [
            { label: 'Show details', name: 'show_details' }
        ]
        
        var actionsRequest = [
            { label: 'Show details', name: 'show_details' },
            { label: 'Delete', name: 'delete' }
        ]
        
        component.set('v.mycolumns', [
            { label: 'Client Name', fieldName: 'Client_Text__c', type: 'text'},
            { label: 'WBS', fieldName: 'WBS_TextValue__c', type: 'text'},
            { label: 'Start Period', fieldName: 'Start_Period__c', type: 'text'},
            { label: 'End Period', fieldName: 'End_Period__c', type: 'text'},
            { label: 'Approver', fieldName: 'Approver_Name__c', type: 'text'},
            { type: 'action', typeAttributes: { rowActions: actionsRequest } }
        ]);
        
        component.set('v.myDuplicatecolumns', [
            { label: 'Name', fieldName: 'Name', type: 'text'},
            { label: 'Client Name', fieldName: 'Client__c', type: 'text'},
            { label: 'WBS', fieldName: 'WBS_Text__c', type: 'text'},
            { label: 'Period', fieldName: 'Start_Period__c', type: 'text'},
            { type: 'action', typeAttributes: { rowActions: actionsDuplicate } }
        ]);
    },
    removeRequest: function (component, row) {
        var rows = component.get('v.requestList');
        var rowIndex = rows.indexOf(row);

        rows.splice(rowIndex, 1);
        component.set('v.requestList', rows);
        component.set("v.bucketSize",component.get('v.requestList').length);
    },
    
    validateClientLookup : function(component, event){
        
       if(typeof document.getElementById('clientName').value == 'undefined' 
          || document.getElementById('clientName').value == null
          || document.getElementById('clientName').value == ''){
           
            document.getElementById('clientFormId').classList.add("slds-has-error");
           	document.getElementById('error-messageClient').style.display = 'block';
            document.getElementById('clientName').classList.add("slds-has-error");
            document.getElementById('clientName').classList.remove("slds-combobox__input");
        }
        else{
            document.getElementById('clientFormId').classList.remove("slds-has-error");
           	document.getElementById('error-messageClient').style.display = 'none';
            document.getElementById('clientName').classList.remove("slds-has-error");
            document.getElementById('clientName').classList.add("slds-combobox__input");
        }
        
    },
    
    validateApproverLookup : function(component, event){
        
        if(typeof document.getElementById('approverName').value == 'undefined' 
          || document.getElementById('approverName').value == null
          || document.getElementById('approverName').value == ''){
            
            document.getElementById('directorFormId').classList.add("slds-has-error");
            document.getElementById('approverName').classList.add("slds-has-error");
            document.getElementById('approverName').classList.remove("slds-combobox__input");
           	document.getElementById('error-messageApprover').style.display = 'block';
  
        } 
        else{
            document.getElementById('directorFormId').classList.remove("slds-has-error");
            document.getElementById('approverName').classList.remove("slds-has-error");
            document.getElementById('approverName').classList.add("slds-combobox__input");
           	document.getElementById('error-messageApprover').style.display = 'none';
        } 
    },
    
    validateForm : function(component, event){
        
        var errorcount = 0;
        
        var validform = true;
        // Show error messages if required fields are blank
        var allValid = component.find('periodField').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        
        if(allValid == false){
           errorcount++; 
        }
        
        
        //console.log(document.getElementById('clientName').value + ' ' + document.getElementById('approverName').value);
        
        if(typeof component.get('v.clientNameId') == 'undefined' 
          || component.get('v.clientNameId') == null
          || component.get('v.clientNameId') == ''){
           
            document.getElementById('clientFormId').classList.add("slds-has-error");
           	document.getElementById('error-messageClient').style.display = 'block';
            document.getElementById('clientName').classList.add("slds-has-error");
            document.getElementById('clientName').classList.remove("slds-combobox__input");
            errorcount++;
        }
        else{
            document.getElementById('clientFormId').classList.remove("slds-has-error");
           	document.getElementById('error-messageClient').style.display = 'none';
            document.getElementById('clientName').classList.remove("slds-has-error");
            document.getElementById('clientName').classList.add("slds-combobox__input");
        }
        
        if(typeof component.get('v.directorLookupId') == 'undefined' 
          || component.get('v.directorLookupId') == null
          || component.get('v.directorLookupId') == ''){
            errorcount++;
            
            document.getElementById('directorFormId').classList.add("slds-has-error");
            document.getElementById('approverName').classList.add("slds-has-error");
            document.getElementById('approverName').classList.remove("slds-combobox__input");
           	document.getElementById('error-messageApprover').style.display = 'block';
  
        } 
        else{
            document.getElementById('directorFormId').classList.remove("slds-has-error");
            document.getElementById('approverName').classList.remove("slds-has-error");
            document.getElementById('approverName').classList.add("slds-combobox__input");
           	document.getElementById('error-messageApprover').style.display = 'none';
        }
        
        if(errorcount > 0){
        	return false;
        }
        else{
            return true;
        }
    }
})