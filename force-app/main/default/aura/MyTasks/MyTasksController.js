({
    doInit: function (component, event, helper) {
        
        var action = component.get('c.getRequests');
        action.setCallback(this, $A.getCallback(function (response) {
            //console.log('Data -> ' + JSON.stringify(response.getReturnValue()));
            var state = response.getState();
            if (state === "SUCCESS") {
                //component.set("v.mydata", response.getReturnValue());
                var requestsArray = [];
                var requests = response.getReturnValue();
                for(var key in requests){
                    requestsArray.push({value:requests[key], key:key});
                }
                component.set("v.mydata", requestsArray);
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                //console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    reviewData : function(component, event, helper){
        var src = event.target || event.srcElement;
        var data = event.target.id;
        
        var array = data.split("$");
        console.log('Array ->' + array);
        
        component.set("v.clientNameId",array[0]);
        component.set("v.clientName",array[1]);
        component.set("v.wbs",array[2]);
        component.set("v.fiscalYear",array[3]);
        component.set("v.period",array[4]);
        component.set("v.showRequests",true);
    },
    back : function(component, event, helper){
        component.set("v.showRequests",false);
    },
})