({
     
    // function call on component Load
    doInit: function(component, event, helper) {
        // create a Default RowItem [Account Instance] on first time Component Load
        // by call this helper function  
        helper.createObjectData(component, event);
    },
     
    // function for save the Records 
    Save: function(component, event, helper) {
        
        // first call the helper function in if block which will return true or false.
        // this helper function check the "Account Name" will not be blank on each row.
        if (helper.validate(component, event,'Save')) {
            // call the apex class method for save the Account List
            // with pass the contact List attribute to method param.  
            var action = component.get("c.SaveAllocatedHours");
            action.setParams({
                "alloc_List": component.get("v.allocatedHoursList")
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    // if response if success then reset the 'allocatedHoursList' Attribute 
                    // and call the common helper method for create a default Object Data to Account List 
                    component.set("v.allocatedHoursList", []);
                    helper.createObjectData(component, event);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : "Success!",
                        type  : "success",
                        message : 'Hours have been splitted successfully!!'
                    });
                    toastEvent.fire();
                    var compEvent = component.getEvent("compEvent");
                    compEvent.setParams({"closePopup": false });
                    compEvent.fire();
                }
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
        }
    },
     
    // function for create new object Row in Contact List 
    addRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        if (helper.validate(component, event, 'Add')) {
        	helper.createObjectData(component, event);
        }
    },
     
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        //get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        //get the all List (allocatedHoursList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.allocatedHoursList");
        
        console.log('AllRowsList 1-> ' + index);
        
        AllRowsList.splice(index, 1);
        //set the allocatedHoursList after remove selected row element  
        console.log('AllRowsList 2-> ' + JSON.stringify(AllRowsList));
        component.set("v.allocatedHoursList", AllRowsList);
    },
})