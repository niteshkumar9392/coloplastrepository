({
    createObjectData: function(component, event) {
        //get the allocatedHoursList from component and add(push) New Object to List 
        
        
        var RowItemList = component.get("v.allocatedHoursList");
        RowItemList.push({
            'sobjectType': 'Allocated_Hours__c',
            'Client__c': component.get("v.clientNameId"),
            'Client_Hours__c': '',
            'Senior_Manager__c': '',
            'Start_Period__c': '',
            'End_Period__c': '',
            'Fiscal_Year__c': component.get("v.fiscalYear"),
            'WBS__c': component.get("v.WBSId")
        });
        
        console.log('RowItemList -> ' + JSON.stringify(RowItemList));
        
        // set the updated list to attribute (allocatedHoursList) again    
        component.set("v.allocatedHoursList", RowItemList);

		var getSMsAction = component.get('c.fetchSeniorManagers');
        getSMsAction.setCallback(this, $A.getCallback(function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.seniorManagers", response.getReturnValue());  
                console.log(JSON.stringify(response.getReturnValue()));
            } 
            else if (state === "ERROR") {
            }
        }));
        $A.enqueueAction(getSMsAction);
        
    },
    //helper function for check if Account Name is not null/blank on save  
    validate: function(component, event, source) {
        var isValid = true;
        var allAllocationRows = component.get("v.allocatedHoursList");
		
        var messages = [];
        var totalHours = 0;
        var totalFinalHours = component.get("v.totalHours");
        
        for (var indexVar = 0; indexVar < allAllocationRows.length; indexVar++) {
			
            if (allAllocationRows[indexVar].Senior_Manager__c == '') {
                isValid = false;
                messages.push('Senior Manager Name Cannot be blank on row number ' + (indexVar + 1));
            }
            if (allAllocationRows[indexVar].Start_Period__c == '') {
                isValid = false;
                messages.push('\nStart Period Cannot be blank on row number ' + (indexVar + 1));
            }
            if (allAllocationRows[indexVar].End_Period__c == '') {
                isValid = false;
                messages.push('\nEnd Period Cannot be blank on row number ' + (indexVar + 1));
            }
            if (allAllocationRows[indexVar].Client_Hours__c == '') {
                isValid = false;
                messages.push('\nClient Hours Cannot be blank on row number ' + (indexVar + 1));
            }
            
            console.log('Nan -> ' + isNaN(allAllocationRows[indexVar].Client_Hours__c));
            if (isNaN(allAllocationRows[indexVar].Client_Hours__c)) {
                isValid = false;
                messages.push('\nClient Hours Should be a number  ' + (indexVar + 1));
            }
            if (allAllocationRows[indexVar].Start_Period__c > allAllocationRows[indexVar].End_Period__c) {
                isValid = false;
                messages.push('\nStart Period Cannot be greater than End Date on row number ' + (indexVar + 1));
            }
            
            totalHours += parseInt(allAllocationRows[indexVar].Client_Hours__c);
        }
        
        for (var i = 0; i < allAllocationRows.length; i++) {
            console.log('Out -> ' + JSON.stringify(allAllocationRows[i]));
            var flag1 = false;
            for (var j = 0; j < allAllocationRows.length; j++) {
                
                if(allAllocationRows[i].Senior_Manager__c != allAllocationRows[j].Senior_Manager__c){
                    if((allAllocationRows[i].Start_Period__c >= allAllocationRows[j].Start_Period__c
                      && allAllocationRows[i].Start_Period__c <= allAllocationRows[j].End_Period__c)
                      || 
                      (allAllocationRows[i].End_Period__c >= allAllocationRows[j].Start_Period__c
                      && allAllocationRows[i].End_Period__c <= allAllocationRows[j].End_Period__c)){
                        
                        flag1 = true;
                        messages.push('\nStart Date and End Date selection range is incorrect.Please select a distinct Range');
                        break;
                    }
                }
            }
            
            if(flag1){
                isValid = false;
                break;
			}
        }
        
        console.log('=> ' + source);
        
        if(source == 'Save' && (totalHours > totalFinalHours ||  totalHours < totalFinalHours)){
            isValid = false;
            messages.push('\nTotal Client Hours must be equal to the Total Hours');
            alert(messages); 
        }
        else if(messages.length > 0){
			alert(messages);            
        }
        
        /*if(messages.length > 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : "Error!",
                type  : "error",
                message : 'This is a required message',
        		messageTemplate: 'Record {0} created! See it {1}!',
                messageTemplateData : messages
            });
            toastEvent.fire();
        }*/
        
        return isValid;
    },
})