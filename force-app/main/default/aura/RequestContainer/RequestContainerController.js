({
    
    cancelReview : function(component, event){
      component.set("v.showRequests",false);  
    },
    
    updateSelectedText: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.requestList",selectedRows);
    },
    
    closeDataPopup : function (component, event, helper) {
        component.set("v.showData",false);
    },
    
    submitRequests : function (component, event, helper) {
        
        if(component.get("v.requestList").length == 0){
        	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success Message',
                message: 'Please select at least one Request !!!',
                type: 'error'
            });
            toastEvent.fire();
            return;
        }
        
        
        var action = component.get('c.submitPeriodRequests');
        action.setParams({
            				requestData : component.get("v.requestList"),
            				directorId : component.get("v.directorLookupId")
                         });
        component.get("v.showSpinner",true);
        action.setCallback(this, $A.getCallback(function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.get("v.showSpinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Request(s) submitted successfully!!!',
                    type: 'success'
                });
                toastEvent.fire();
                helper.getPeriodRequestHelper(component, event, helper);
            } else if (state === "ERROR") {
                var errors = response.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error Message',
                    message: ''+JSON.stringify(errors),
                    type: 'error'
                });
                toastEvent.fire();
            }
        }));
        $A.enqueueAction(action); 
    },
    
    getPeriodRequest : function (component, event, helper) {
        helper.getPeriodRequestHelper(component, event);
        /*var obj1 = {};
        obj1.label = component.get("v.wbs");
        obj1.value = component.get("v.wbs");
        array.push(obj1);
        console.log(array);*/
        //component.set("v.wbslist",array);
    },

    handleRowAction: function (component, event, helper) {
        //var action = event.getParam('action');
        var row = event.getParam('row');
		component.set("v.periodId",row.Id);
        component.set("v.showData",true);
        
       
    },
    
    searchAccount : function(component, event, helper){
        
        component.set("v.clientNameId",'');
        
        component.set("v.searchClients",true);
        var src = event.target || event.srcElement;
        var searchText = event.target.value;
        if(searchText.length > 2){
            console.log(searchText.length);
            var action = component.get('c.searchClients');
            action.setParams({ searchText : searchText});           
            var cmpTarget = component.find('clientlookup');
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.clients", response.getReturnValue());
                    component.set("v.dataFound",true);
                    $A.util.addClass(cmpTarget, 'slds-is-open');
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": "Something went wrong, plese check with your system administrator."
                    });
                    $A.util.removeClass(cmpTarget, 'slds-is-open');
                    component.set("v.dataFound",false);
                    resultsToast1.fire();
                }
                    else{
                        $A.util.removeClass(cmpTarget, 'slds-is-open');
                        component.set("v.dataFound",false);
                    }
            }));
            $A.enqueueAction(action);
        }
    },
    
    selectAccount : function(component, event, helper){
        component.set("v.clientNameId",null);
        component.set("v.clientName",null);
        
        component.set("v.showSpinner",true);
        
        var src = event.target || event.srcElement;
        var selectedClient = event.target.id;
        var clients = component.get("v.clients");
        
        var array = [];
        
        for(var i = 0 ; i < clients.length ; i++){
            if(clients[i].Id === selectedClient){
                component.set("v.clientNameId",clients[i].Id);
                component.set("v.clientName",clients[i].Name);
   
                if(typeof clients[i].WBS__r != 'undefined'){
                    var obj1 = {};
                    obj1.label = '--None--';
                    obj1.value = null;
                    array.push(obj1); 
                    
                    for(var j = 0 ; j < clients[i].WBS__r.length ; j++){
                        var obj = {};
                        if(typeof clients[i].WBS__r[j] != 'undefined'){
                            obj.label = clients[i].WBS__r[j].Name;    
                            obj.value = clients[i].WBS__r[j].Id;
                        }
                        array.push(obj);  
                    } 
                }
                helper.validateClientLookup(component,event);
                break;
                component.set("v.clients", []);
            }
        }
        
        component.set("v.wbslist",array);
        var cmpTarget = component.find('clientlookup');
        $A.util.removeClass(cmpTarget, 'slds-is-open');
        component.set("v.showSpinner",false);
        component.set("v.searchClients",false);
    },
    
    searchApprovers : function(component, event, helper){
        
        component.set("v.directorLookupId",'');
        
        component.set("v.searchDirectors",true);
        var src = event.target || event.srcElement;
        var searchText = event.target.value;
        
        if(searchText.length > 2){
            var action = component.get('c.searchDirectors');
            action.setParams({ searchText : searchText});           
            var cmpTarget = component.find('approverlookup');
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.approvers", response.getReturnValue());
                    $A.util.addClass(cmpTarget, 'slds-is-open');
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": "Something went wrong, plese check with your system administrator."
                    });
                    $A.util.removeClass(cmpTarget, 'slds-is-open');
                    resultsToast1.fire();
                }
                    else{
                        $A.util.removeClass(cmpTarget, 'slds-is-open');
                    }
            }));
            $A.enqueueAction(action);
        }
    },
    
    selectApprover : function(component, event, helper){
        
        component.set("v.directorLookupId",null);
        component.set("v.directorName",null);
        
        var src = event.target || event.srcElement;
        var selectedClient = event.target.id;
        var approvers = component.get("v.approvers");
        
        for(var i = 0 ; i < approvers.length ; i++){
            if(approvers[i].Id === selectedClient){
                console.log('Test ' + approvers[i].Name);
                component.set("v.directorLookupId",approvers[i].Id);
                component.set("v.directorName",approvers[i].Name);
                component.set("v.searchDirectors",false);
                helper.validateApproverLookup(component,event);
                break;
            }
        }
        var cmpTarget = component.find('approverlookup');
        $A.util.removeClass(cmpTarget, 'slds-is-open');
        component.set("v.approvers", []);
    },
    
    
    approveRequests : function (component, event, helper) {
        
        if(component.get("v.requestList").length == 0){
        	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success Message',
                message: 'Please select at least one Request !!!',
                type: 'error'
            });
            toastEvent.fire();
            return;
        }
        
        
        var action = component.get('c.approvePeriodRequests');
        action.setParams({
            				requestData : component.get("v.requestList"),
                         });
        component.get("v.showSpinner",true);
        action.setCallback(this, $A.getCallback(function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.get("v.showSpinner",false);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Request(s) has been approved successfully!!!',
                    type: 'success'
                });
                toastEvent.fire();
                helper.getPeriodRequestHelper(component, event, helper);
            } else if (state === "ERROR") {
                var errors = response.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error Message',
                    message: ''+JSON.stringify(errors),
                    type: 'error'
                });
                toastEvent.fire();
            }
        }));
        $A.enqueueAction(action); 
    },
    rejectRequests : function (component, event, helper) {
        
        if(component.get("v.requestList").length == 0){
        	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success Message',
                message: 'Please select at least one Request !!!',
                type: 'error'
            });
            toastEvent.fire();
            return;
        }
        
        
        var action = component.get('c.rejectPeriodRequests');
        component.get("v.showSpinner",true);
        action.setParams({
            				requestData : component.get("v.requestList"),
                         });
        
        action.setCallback(this, $A.getCallback(function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.get("v.showSpinner",false);
                var errors = response.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: 'Request(s) has been approved successfully!!!',
                    type: 'success'
                });
                toastEvent.fire();
                helper.getPeriodRequestHelper(component, event, helper);
            } else if (state === "ERROR") {
                var errors = response.getError();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error Message',
                    message: ''+JSON.stringify(errors),
                    type: 'error'
                });
                toastEvent.fire();
            }
        }));
        $A.enqueueAction(action); 
    },
})