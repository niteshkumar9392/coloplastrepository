({
    getPeriodRequestHelper : function (component, event, helper) {
        var actions = [
            { label: 'Show details', name: 'show_details' }
        ];
        
        component.set('v.columns', [
            { label: 'Start Period', fieldName: 'Start_Period__c', type: 'text'},
            { label: 'End Period', fieldName: 'End_Period__c', type: 'text'},
            { label: 'Portfolio', fieldName: 'Portfolio__c', type: 'text'},
            { label: 'Marketing Offering', fieldName: 'Market_Offering__c', type: 'text'},
            { label: 'Employee Competency Area', fieldName: 'Employee_Competency_Area__c', type: 'text'},
            { label: 'Employee Capability', fieldName: 'Employee_Capability__c', type: 'text'},
            { label: 'Request By', fieldName: 'Requested_by__c', type: 'text'},
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
        
        var action = component.get('c.getRequests');
        
        console.log(component.get("v.clientNameId"));
        console.log(component.get("v.wbs"));
        console.log(component.get("v.fiscalYear"));
        console.log(component.get("v.userProfile"));
        console.log(component.get("v.period"));
        
        action.setParams({
            clientId : component.get("v.clientNameId"),
            WBS : component.get("v.wbs"),
            fiscalYear : component.get("v.fiscalYear"),
            profileName : component.get("v.userProfile"),
            period : component.get("v.period")
        });
        
        action.setCallback(this, $A.getCallback(function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set("v.requestMap", result);
                console.log(JSON.stringify(result));
                if(result.length > 0){
                    component.set("v.showRequests",true);
                    console.log('ShowReq ' + showSearchSection);
                    component.set("v.NoResult",false);
                    
                }
                else{
                    component.set("v.showRequests",false);
                    if(component.get("v.showSearchSection") == true){
                        component.set("v.NoResult",true);
                    }
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action); 
    },
    removeBook: function (cmp, row) {
        var rows = cmp.get('v.data');
        var rowIndex = rows.indexOf(row);
        
        rows.splice(rowIndex, 1);
        cmp.set('v.data', rows);
    },
    validateClientLookup : function(component, event){
        
        if(typeof document.getElementById('clientName').value == 'undefined' 
           || document.getElementById('clientName').value == null
           || document.getElementById('clientName').value == ''){
            
            document.getElementById('clientFormId').classList.add("slds-has-error");
            document.getElementById('error-messageClient').style.display = 'block';
            document.getElementById('clientName').classList.add("slds-has-error");
            document.getElementById('clientName').classList.remove("slds-combobox__input");
        }
        else{
            document.getElementById('clientFormId').classList.remove("slds-has-error");
            document.getElementById('error-messageClient').style.display = 'none';
            document.getElementById('clientName').classList.remove("slds-has-error");
            document.getElementById('clientName').classList.add("slds-combobox__input");
        } 
    },
    validateApproverLookup : function(component, event){
        
        if(typeof document.getElementById('approverName').value == 'undefined' 
           || document.getElementById('approverName').value == null
           || document.getElementById('approverName').value == ''){
            
            document.getElementById('directorFormId').classList.add("slds-has-error");
            document.getElementById('approverName').classList.add("slds-has-error");
            document.getElementById('approverName').classList.remove("slds-combobox__input");
            document.getElementById('error-messageApprover').style.display = 'block';
            
        } 
        else{
            document.getElementById('directorFormId').classList.remove("slds-has-error");
            document.getElementById('approverName').classList.remove("slds-has-error");
            document.getElementById('approverName').classList.add("slds-combobox__input");
            document.getElementById('error-messageApprover').style.display = 'none';
        } 
    }
})