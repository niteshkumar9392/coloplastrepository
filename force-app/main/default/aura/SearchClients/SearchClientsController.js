({
    
    openSplitScreen : function(component, event, helper){
        console.log(component.get("v.clientNameId"));
      	component.set("v.popupTotalHours",component.get("v.totalHours"));
      	component.set("v.showSplitScreen",true);   
    },
    
    closePopup : function(component, event, helper){
      	component.set("v.showSplitScreen",false);  
    },
    
    handleComponentEvent : function(component, event, helper){
       console.log('Popup -> ' + JSON.stringify(event.getParams("closePopup")));
       component.set("v.showSplitScreen", event.getParams("closePopup").closePopup); 
       helper.searchClientHoursHelper(component, event);
    },
    
    splitWBS : function(component, event, helper){
        var clickedBtn = event.target.id;
        var data = clickedBtn.split('#');
        component.set("v.selectedWBS",data[0]);
        component.set("v.popupTotalHours",data[1]);
      	component.set("v.showSplitScreen",true);  
    },
    
    searchClientHours : function(component, event, helper){
        helper.searchClientHoursHelper(component, event);
    },
    searchAccount : function(component, event, helper){
        
        component.set("v.clientNameId",'');
        
        component.set("v.searchClients",true);
        var src = event.target || event.srcElement;
        var searchText = event.target.value;
        if(searchText.length > 2){
            console.log(searchText.length);
            var action = component.get('c.searchClients');
            action.setParams({ searchText : searchText});           
            var cmpTarget = component.find('clientlookup');
            
            action.setCallback(this, $A.getCallback(function (response) {
                
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.clients", response.getReturnValue());
                    component.set("v.dataFound",true);
                    $A.util.addClass(cmpTarget, 'slds-is-open');
                } else if (state === "ERROR") {
                    var resultsToast1 = $A.get("e.force:showToast");
                    resultsToast1.setParams({
                        "title": "ERROR",
                        "type":"error",
                        "message": "Something went wrong, plese check with your system administrator."
                    });
                    $A.util.removeClass(cmpTarget, 'slds-is-open');
                    resultsToast1.fire();
                }
                    else{
                        $A.util.removeClass(cmpTarget, 'slds-is-open');
                    }
            }));
            $A.enqueueAction(action);
        }
    },
    
    selectAccount : function(component, event, helper){
        component.set("v.clientNameId",null);
        component.set("v.clientName",null);
        
        component.set("v.showResult",false);
        
        var src = event.target || event.srcElement;
        var selectedClient = event.target.id;
        var clients = component.get("v.clients");
        
        var array = [];
        
        for(var i = 0 ; i < clients.length ; i++){
            if(clients[i].Id === selectedClient){
                component.set("v.clientNameId",clients[i].Id);
                component.set("v.clientName",clients[i].Name);
                helper.validateClientLookup(component,event);
                break;
                component.set("v.clients", []);
            }
        }
        
        var cmpTarget = component.find('clientlookup');
        $A.util.removeClass(cmpTarget, 'slds-is-open');
        component.set("v.searchClientsHours",false);
    }
})