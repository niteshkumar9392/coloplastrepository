({
    searchClientHoursHelper : function(component, event, helper){
        
        component.set("v.searchLabel", "Searching...");
        
        var action = component.get('c.searchPreAllocatedHours');
        action.setParams({ clientId : component.get('v.clientNameId'),
                          fiscalYear : component.get('v.fiscalYear')
                         });           
        
        action.setCallback(this, $A.getCallback(function (response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.clientsHours", response.getReturnValue().preAllocations);
                //console.log('Date : ' + JSON.stringify(response.getReturnValue()));
                
                var totalClientHours = 0;
                var allocatedHours = 0;
                var hoursDifference = 0;
                
                for (var i = 0; i < response.getReturnValue().preAllocations.length; i++) {
                    if(response.getReturnValue().preAllocations[i].Client_Hours__c != null){
                        totalClientHours += response.getReturnValue().preAllocations[i].Client_Hours__c;
                    }
                }
                
                
                // var allocationMap = [];
                // var allocationRec = {};
                
                
                component.set("v.allocatedHourList",response.getReturnValue().alloted_hours);
                
                console.log('Allocated Hours -> ' + response.getReturnValue().alloted_hours);
                
                for (var i = 0; i < response.getReturnValue().alloted_hours.length; i++) {
                    if(response.getReturnValue().alloted_hours[i].Client_Hours__c != null){
                        allocatedHours += response.getReturnValue().alloted_hours[i].Client_Hours__c;
                    }
                }
                
                hoursDifference = totalClientHours - allocatedHours;
                
                console.log(totalClientHours + ' ' + allocatedHours + ' ' + hoursDifference);
                
                component.set("v.totalHours",totalClientHours);
                component.set("v.allocatedHours",allocatedHours);
                component.set("v.hoursRemaining",hoursDifference);
                
                if(totalClientHours > 0 || allocatedHours > 0){
                    component.set("v.showResult",true);
                }
                
                
                var action1 = component.get('c.getPeriodWiseData');
                action1.setParams({ clientName : component.get('v.clientName'),
                                   fiscalYear : component.get('v.fiscalYear')
                                  });           
                
                action1.setCallback(this, $A.getCallback(function (response) {
                    
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        //component.set("v.periodWiseHours", response.getReturnValue());
                        console.log('periodWiseHours -> ' + JSON.stringify(response.getReturnValue()));
                        
                        var arrayToStoreKeys=[];
                        var response = response.getReturnValue();
                        for(var key in response){
                            
                            arrayToStoreKeys.push({value:response[key], key:key});
                        }
                        
                        component.set('v.periodWiseHours',arrayToStoreKeys); 
                        component.set("v.searchLabel", "Search");
                        
                        
                    } else if (state === "ERROR") {
                        var resultsToast1 = $A.get("e.force:showToast");
                        resultsToast1.setParams({
                            "title": "ERROR",
                            "type":"error",
                            "message": "Something went wrong, plese check with your system administrator."
                        });
                        resultsToast1.fire();
                        component.set("v.NoResult",true);
                    }
                        else{
                            //TODO
                        }
                }));
                $A.enqueueAction(action1);
                
            } else if (state === "ERROR") {
                var resultsToast1 = $A.get("e.force:showToast");
                resultsToast1.setParams({
                    "title": "ERROR",
                    "type":"error",
                    "message": "Something went wrong, plese check with your system administrator."
                });
                resultsToast1.fire();
                component.set("v.NoResult",true);
            }
                else{
                    component.set("v.NoResult",true);
                }
        }));
        $A.enqueueAction(action);  
        
    },
    
    validateClientLookup : function(component, event){
        
        if(typeof document.getElementById('clientName').value == 'undefined' 
           || document.getElementById('clientName').value == null
           || document.getElementById('clientName').value == ''){
            
            document.getElementById('clientFormId').classList.add("slds-has-error");
            document.getElementById('error-messageClient').style.display = 'block';
            document.getElementById('clientName').classList.add("slds-has-error");
            document.getElementById('clientName').classList.remove("slds-combobox__input");
        }
        else{
            document.getElementById('clientFormId').classList.remove("slds-has-error");
            document.getElementById('error-messageClient').style.display = 'none';
            document.getElementById('clientName').classList.remove("slds-has-error");
            document.getElementById('clientName').classList.add("slds-combobox__input");
        } 
    }
})