public class AllocationWrapper {

    @AuraEnabled
    public List<Allocated_Hours__c > alloted_hours;
    
    @AuraEnabled 
    public List<Pre_Allocation__c> preAllocations;
    
    public AllocationWrapper(List<Allocated_Hours__c > alloted_hours, List<Pre_Allocation__c> preAllocations){
        //Assign the values to the class variables.
        this.alloted_hours = alloted_hours;
        this.preAllocations = preAllocations;
    }
}