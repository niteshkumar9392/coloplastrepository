public with sharing class AuraSampleController{
    @AuraEnabled
    public static void SaveAllocatedHours(List<Allocated_Hours__c > alloc_List){
        for(Allocated_Hours__c acc_hour : alloc_List){
            acc_hour.Director__c = UserInfo.getUserId();
            acc_hour.Ownerid = acc_hour.Senior_Manager__c;
        }
        insert alloc_List;
    }
    
    @AuraEnabled
    public static List<SelectOption> fetchSeniorManagers(){
        
        List<SelectOption> seniorManagers = new List<SelectOption>();
        
        for(User userObj : [Select Id, Email, Name from User where Profile.Name = 'Senior Manager' AND isActive = true]){
            String label = userObj.Name + ' <' + userObj.Email +'>';
            seniorManagers.add(new SelectOption(label , userObj.Id));
        }
        
        return seniorManagers;
    }
}