public with sharing class ExistingRequestController {
	
    @AuraEnabled
    public static List<Account> searchClients(String searchText){
        string tempInput = '%' + searchText + '%';
        return [Select Id, Name, (Select Name from WBS__r) from Account WHERE Name LIKE :tempInput];
    }
    
    @AuraEnabled
    public static List<User> searchDirectors(String searchText){
        string tempInput = '%' + searchText + '%';
        return [Select Id, Name, Profile.Name from User WHERE Name LIKE :tempInput and Profile.Name = 'Director'];
    }
    
    @AuraEnabled
    public static void submitPeriodRequests(List<Period__c> requestData, String directorId){
        
        for(Period__c period : requestData){
           period.Status__c = 'Pending Approval';
           period.Requested_by__c  = UserInfo.getName();
           period.OwnerId = UserInfo.getUserId();
           period.Requested_by_Id__c = UserInfo.getUserId();
           period.Director__c = directorId; 
        }
        
        update requestData;
    }
    @AuraEnabled
    public static void approvePeriodRequests(List<Period__c> requestData){
        
        for(Period__c period : requestData){
           period.Status__c = 'Approved';
        }
        
        system.debug('Test -> ' + JSON.serializePretty(requestData));
        update requestData;
    }
    
    @AuraEnabled
    public static void rejectPeriodRequests(List<Period__c> requestData){
        
        for(Period__c period : requestData){
           period.Status__c = 'Rejected';
        }
        
        system.debug('Test -> ' + JSON.serializePretty(requestData));
        update requestData;
    }
    
    @AuraEnabled
    public static List<Period__c> getRequests(String clientId, String WBS, String fiscalYear, String profileName, String period){
        
        //Map<String,List<Period__c>> requestMap = new Map<String,List<Period__c>>();
        if(profileName == 'Director' || profileName == 'System Administrator'){
        	return [Select Name, Start_Period__c, End_Period__c, Fiscal_Year__c, Client_Text__c, 
                WBS_TextValue__c, Portfolio__c,Requested_by__c , Market_Offering__c, Employee_Competency_Area__c, 
                Employee_Capability__c 
                FROM Period__c 
                WHERE RecordType.Name = 'Request'
                AND Client_Name__c = :clientId
                AND WBS__c = :WBS
                AND Start_Period__c = :period
                AND Fiscal_Year__c = :fiscalYear
                AND (Status__c = 'Pending Approval') ORDER BY Start_Period__c ASC];    
        } 
        else{
            return [Select Name, Start_Period__c, End_Period__c, Fiscal_Year__c, Client_Text__c, 
                WBS_TextValue__c, Portfolio__c,Requested_by__c , Market_Offering__c, Employee_Competency_Area__c, 
                Employee_Capability__c 
                FROM Period__c 
                WHERE RecordType.Name = 'Request'
                AND Client_Name__c = :clientId
                AND WBS__c = :WBS
                AND Start_Period__c = :period
                AND Fiscal_Year__c = :fiscalYear
                AND (Status__c = 'New' OR Status__c = 'Rejected') ORDER BY Start_Period__c ASC];
        }
    }
}