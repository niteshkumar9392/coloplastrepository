public class MH_ManagedHourTriggerHandler {
    
    public void afterInsert(){
        List<Managed_Hours__c> lstMangedHours = new List<Managed_Hours__c>();
        lstMangedHours= Trigger.new;
        System.debug('==lstMangedHours=='+lstMangedHours);
        Map<String, RevenueHoursWrapper> AggregateResult = new Map<String, RevenueHoursWrapper>();
        Set<String> setUniqueKey = new Set<String>();
        for(Managed_Hours__c currManagedHour : lstMangedHours){
            setUniqueKey.add(currManagedHour.Unique_Key__c);
            Decimal TotalHours;
            Decimal TotalRevenue;
            RevenueHoursWrapper newWrapper = new RevenueHoursWrapper() ;
            if(AggregateResult.containsKey(currManagedHour.Unique_Key__c)){
                newWrapper = AggregateResult.get(currManagedHour.Unique_Key__c);
                TotalHours = newWrapper.totalHours + currManagedHour.Client_Service_Hours__c;
                TotalRevenue = newWrapper.totalRevenue + currManagedHour.Net_Revenue__c;
                newWrapper.totalHours = TotalHours;
                newWrapper.totalRevenue = TotalRevenue;
                AggregateResult.put(currManagedHour.Unique_Key__c,newWrapper);
            }
            else{
                TotalHours = currManagedHour.Client_Service_Hours__c;
                TotalRevenue = currManagedHour.Net_Revenue__c;
                newWrapper.totalHours = TotalHours;
                newWrapper.totalRevenue = TotalRevenue;
                AggregateResult.put(currManagedHour.Unique_Key__c,newWrapper);
            }
        }
        System.debug('==AggregateResult=='+AggregateResult);
        System.debug('==setUniqueKey=='+setUniqueKey);
        List<Period__c> lstPeriods = new List<Period__c>();
        List<Period__c> updatedPeriods = new List<Period__c>();
        lstPeriods = [SELECT Id,Status__c,Unique_Key__c ,Total_Client_Hours__c,MH_Data_Status__c ,
                      Total_Net_Revenue__c from Period__c where Unique_Key__c IN :setUniqueKey And RecordType.Name != 'Request' ];
        System.debug('==lstPeriods=='+lstPeriods);
        for(Period__c currPeriod : lstPeriods){
            RevenueHoursWrapper newWrapper = new RevenueHoursWrapper() ;
            if(AggregateResult.containsKey(currPeriod.Unique_Key__c)){
                newWrapper =  AggregateResult.get(currPeriod.Unique_Key__c); 
                if(currPeriod.Total_Client_Hours__c!= NUll){
                currPeriod.Total_Client_Hours__c = currPeriod.Total_Client_Hours__c + newWrapper.totalHours;
                    }
                else{
                    currPeriod.Total_Client_Hours__c = newWrapper.totalHours;
                }
                if(currPeriod.Total_Net_Revenue__c!= NUll){
                currPeriod.Total_Net_Revenue__c = currPeriod.Total_Net_Revenue__c + newWrapper.totalRevenue;
                }
                else{
                    currPeriod.Total_Net_Revenue__c = newWrapper.totalRevenue;
                }
                    
                if(currPeriod.MH_Data_Status__c == 'Awaiting Data'){
                    currPeriod.MH_Data_Status__c = 'Data Captured';
                }
            }
            updatedPeriods.add(currPeriod);
        }
        System.debug('==updatedPeriods=='+updatedPeriods);
        try{
            update updatedPeriods;
        }
        catch(Exception e){
            System.debug('Your code ran into the following exception :- '+e);
        }
    }
}