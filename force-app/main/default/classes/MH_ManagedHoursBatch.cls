global class MH_ManagedHoursBatch implements Database.Batchable<sObject>, Database.Stateful{
    global Map<String, RevenueHoursWrapper> AggregateResult = new Map<String, RevenueHoursWrapper>();
    global DateTime yesterDayDateTime = System.now().addDays(-1);
    global DateTime currDayDateTime = System.now();
    //global String FiscalYear ='2019002';
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT ID,Fiscal_Year_Period__c ,Employee_Capability__c,Profit_Center_Business_Line__c,WBS_Level_1__c , UniqueText__c,LastModifiedDate  ,Net_Revenue__c ,Client_Service_Hours__c,'+
        //    'Client_Name__c,Unique__c,SM_D__c , Profit_Center_Business_Area__c,Employee_Competency_Area__c FROM Managed_Hours__c ';
            'Client_Name__c,Unique__c,SM_D__c , Profit_Center_Business_Area__c,Employee_Competency_Area__c FROM Managed_Hours__c WHERE (LastModifiedDate < :currDayDateTime AND LastModifiedDate  >= :yesterDayDateTime) ' ;
                   
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Managed_Hours__c> lstMangedHours){
        Map<string,Period__c> mapRequest = new Map<string,Period__c>();
        Map<String,Managed_Hours__c> mapMH = new Map<String,Managed_Hours__c>();
        Set<String> setUniqueKey = new Set<String>();
        Set<String> clientNames = new Set<String>();
        Set<String> setWBS = new Set<String>();
        Map<String,String> wbsToClient = new Map<String,String>();
        Map<String,String> newWbsToClient = new Map<String,String>();
        for(Managed_Hours__c currManagedHour : lstMangedHours){
            setUniqueKey.add(currManagedHour.UniqueText__c);
            mapMH.put(currManagedHour.UniqueText__c, currManagedHour);
            setWBS.add(currManagedHour.WBS_Level_1__c);
            clientNames.add(currManagedHour.Client_Name__c);
            //Map to define a wbs and Client relationship
            wbsToClient.put(currManagedHour.WBS_Level_1__c,currManagedHour.Client_Name__c);
        }
        //retriving already present Accounts
        List<Account>  lstAccount = new List<Account>();
        List<String> lstClientNames = new List<String>();
        Map<String, Id> mapPresentAccount = new Map<String,Id>();
            for(Account acc: [SELECT Id, Name FROM Account 
                              WHERE (RecordType.Name = 'MH Account') 
                              AND (Name IN :clientNames) LIMIT 50000]){
                lstClientNames.add(acc.Name);
                mapPresentAccount.put(acc.Name,acc.Id);
            }
        
        //WBS Addition Logic
        List<WBS__c> lstNewWBS = new List<WBS__c>();
        List<String> allWBS = new List<String>();
        Map<string,Account> AccMap = new Map<String, Account>();
        Map<String,Id> mapWBS = new Map<String,Id>();       
        for(WBS__c currWBS : [SELECT Id,Name,Client_Name__c  from WBS__c]){
            allWBS.add(currWBS.Name);
            mapWBS.put(currWBS.Name, currWBS.Id);
        }
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MH Account').getRecordTypeId();
        //check if the list contains the WBS present in this batch class
        for(String currWBS : setWBS){
            if(!allWBS.contains(currWBS)){
                Account newAcc = new Account();
                
                //New WBS is recieved 
                String clientName = wbsToClient.get(currWBS);
                WBS__c newWBS = new WBS__c();
                newWBS.Name = currWBS ;
                //check if the client for this WBS is present in the system, and if not add the client record as well
                if(!lstClientNames.contains(clientName)){
                    System.debug('New Account present');
                    newAcc.Name = clientName;
                    newAcc.RecordTypeId = accountRecordTypeId;
                    newAcc.Status__c = 'Active';
                    AccMap.put(currWBS,newAcc);
                    newWBS.Client__c = newAcc.Name;
                }
                else{
                    newWBS.Client_Name__c = mapPresentAccount.get(clientName);
                    System.debug('No new Account present');
                }
                
                //Adding new WBS to the system
                lstNewWBS.add(newWBS);
            }
            else{
                System.debug('No new WBS present');
            }
        }
        //Insert New Account-WBS DML
        try{
            if(AccMap.values().size()>0){
                insert AccMap.values();
            }
            
            if(lstNewWBS.size()>0){
                insert lstNewWBS;
            }
        }
        Catch(exception e){
            System.debug('==Following is the exception that you have recieved - '+e);
        }
        
        //Creating new Request Records.
        for(Account acc: [Select Id, Name  from Account Where RecordType.Name = 'MH Account' AND (Name NOT IN :lstClientNames)]){
            mapPresentAccount.put(acc.Name,acc.Id);
        }
        Map<String,Id> mapDirectorName = new Map<String,Id>();
        for(User usr: [Select Id, Name, Profile.Name from User WHERE Profile.Name = 'Director']){
            mapDirectorName.put(usr.Name, usr.Id);
        }
        for(WBS__c currWBS : [SELECT Id,Name from WBS__c Where (Id NOT IN :mapWBS.values())]){
            allWBS.add(currWBS.Name);
            mapWBS.put(currWBS.Name, currWBS.Id);
        }
        List<Period__c> newLstRequestRecords = new List<Period__c>();
        Id periodRecordTypeId = Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Approved Request').getRecordTypeId();
        Id periodRequestRecordTypeId = Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Request').getRecordTypeId();
        for(Period__c currPeriod:[Select Id, RecordTypeId, Unique_Key__c from Period__c 
                                  where (RecordTypeId = :periodRequestRecordTypeId OR RecordTypeId = :periodRecordTypeId) 
                                  AND (Unique_Key__c IN :setUniqueKey) LIMIT 50000]){
            mapRequest.put(currPeriod.Unique_Key__c,currPeriod);
        }
        for(String curruniqueKey : setUniqueKey){
            //checking if the unique key is already present in the system or not
            if(!mapRequest.containsKey(curruniqueKey)){
                //Adding new Request Records.
                Period__c newRequestRecord = new Period__c();
                String uniqueKey = mapMH.get(curruniqueKey).Unique__c;
                List<String> uniqueKeys = uniqueKey.split('#');
                newRequestRecord.Client_Name__c = mapPresentAccount.get(uniqueKeys[1]);
                newRequestRecord.WBS__c=mapWBS.get(uniqueKeys[2]);
                newRequestRecord.Fiscal_Year__c=uniqueKeys[0].mid(0,4);
                newRequestRecord.Director__c= mapDirectorName.get(mapMH.get(curruniqueKey).SM_D__c);
                newRequestRecord.Start_Period__c=uniqueKeys[0].mid(4,3);
                newRequestRecord.End_Period__c=uniqueKeys[0].mid(4,3);
                newRequestRecord.Portfolio__c = mapMH.get(curruniqueKey).Profit_Center_Business_Area__c;
                newRequestRecord.Market_Offering__c = mapMH.get(curruniqueKey).Profit_Center_Business_Line__c;
                newRequestRecord.Employee_Competency_Area__c= mapMH.get(curruniqueKey).Employee_Competency_Area__c;
                newRequestRecord.Employee_Capability__c= mapMH.get(curruniqueKey).Employee_Capability__c;
                newRequestRecord.RecordTypeId=periodRequestRecordTypeId;
                newRequestRecord.Status__c = 'New'; 
                newLstRequestRecords.add(newRequestRecord);
            }
            else{
                System.debug('No new Request is required to be inserted');
            }
        }
        
        //inserting new request record
        if(newLstRequestRecords.size()>0){
            insert newLstRequestRecords;
        }
        else{
            System.debug('No new Request');
        }
        
    }
    global void finish(Database.BatchableContext bc){
       //No logic required for now
    }
}