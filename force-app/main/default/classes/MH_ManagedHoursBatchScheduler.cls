global class MH_ManagedHoursBatchScheduler implements Schedulable{
	global void execute(SchedulableContext ctx) {
    	MH_ManagedHoursBatch mhb = new MH_ManagedHoursBatch();
        Database.executeBatch(mhb);
    }
}