public class MH_MyTeamHandler{

    @auraenabled
    public static List<MyTeamWrapper> searchClients(String searchString){
        Map<Id, MyTeamWrapper> accounts = new Map<Id, MyTeamWrapper>();
        searchString = '%'+searchString+'%';
        List<Account> searchResults = [Select   Id, 
                                                Name, 
                                                (Select Name from WBS__r) 
                                                FROM Account 
                                                WHERE Name LIKE :searchString];
        for(Account account : searchResults ){
            MyTeamWrapper wrapper = new MyTeamWrapper();
            wrapper.account = account;
            accounts.put(account.Id,wrapper);
        }
        if(!accounts.isEmpty()){
            return accounts.values();
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static List<MyTeamWrapper> fetchAccoutMembers(Id accountId){
        Map<String, MyTeamWrapper> teamMembersMap = new Map<String, MyTeamWrapper>();
        List<MyTeamWrapper> teamMembers = new List<MyTeamWrapper>();
        List<Managed_Hours__c> hoursResult = [Select    Id, 
                                                        Employee_Email__c, 
                                                        Employee_Name__c, 
                                                        Employee_Job_Level__c,
                                                        Client__c
                                                        FROM Managed_Hours__c
                                                        WHERE Client__c=:accountId];
        for(Managed_Hours__c managedHour : hoursResult){
            MyTeamWrapper wrapper = new MyTeamWrapper();
            wrapper.fullName = managedHour.Employee_Name__c;
            wrapper.emailAddress = managedHour.Employee_Email__c;
            wrapper.jobLevel = managedHour.Employee_Job_Level__c;
            teamMembersMap.put(managedHour.Employee_Email__c,wrapper);
        }                                             
        if(!teamMembersMap.isEmpty()){
            system.debug(teamMembersMap.size());
            return teamMembersMap.values();
        }else{
            return null;
        }
    }
    

    //Wrapper Class for MyTeam details
    public class MyTeamWrapper{
        public Account account {get; set;}
        Public String fullName {get; set;}
        public String emailAddress {get; set;}
        public String jobLevel {get; set;}
    
            public MyTeamWrapper(Account account) {
                this.account = account;
            }
            
            public MyTeamWrapper(){
            }
    }


}