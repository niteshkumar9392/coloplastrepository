/* Author: Vinit
* Assumptions :1. There are no period records already present in the system.
*               2. While the upload of data is happening there no two duplicate records present in the upload.
*               3. The WBS and Account Salesforce Id's are uploaded in the data which is uploaded.
*               4. The Account and WBS records should already be present in the system.
*               5. If there is a record present of the record type as request and status not equals rejected, then the child record would be present.
*/ 
global class MH_OnLoadPeriodBatch implements Database.Batchable<sObject>{
    global DateTime yesterDayDateTime = System.now().addDays(-1);
    global DateTime currDayDateTime = System.now();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Id periodRecordTypeId = Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Request').getRecordTypeId();
        String sourceString = 'Data Upload';
        String status = 'Approved';
        return Database.getQueryLocator('Select Fiscal_Year__c ,Start_Period__c ,Client_Name__c,WBS__c,RecordTypeId,Status__c,Client__c,WBS_Text__c,'+
                                        'Portfolio__c,Market_Offering__c ,Employee_Competency_Area__c ,'+
                                        'Capability__c,Employee_Capability__c,Unique_Key__c , SM_D__c ,Director__c, End_Period__c  from Period__c  '+
                                        'WHERE LastModifiedDate < :currDayDateTime AND LastModifiedDate  >= :yesterDayDateTime '+
                                        'AND Request_Source__c= :sourceString AND Status__c= :status AND RecordTypeId= :periodRecordTypeId  ');
    }
    global void execute(Database.BatchableContext bc, List<Period__c> lstPeriod){
        System.debug('==lstPeriod.size()=='+lstPeriod.size());
        System.debug('==lstPeriod=='+lstPeriod);  
        
        //creating unique key && checking duplicate records.
        List<String> uniqueKeys = MH_Utilities.prepareUniqueKeys(lstPeriod,'Approved');
        List<Period__c> duplicatePeriods = MH_Utilities.checkDuplicatPeriods(uniqueKeys);
        Set<String> setDuplicateRecords = new Set<String>();
        for(Period__c curP : duplicatePeriods){
            setDuplicateRecords.add(curP.Unique_Key__c);
        }
        System.debug('==uniqueKeys=='+uniqueKeys);
        System.debug('==duplicatePeriods=='+duplicatePeriods);
        
        //getting the record type
        Id periodRecordTypeId = Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Approved Period').getRecordTypeId();
        
        List<Period__c> lstNewPeriod = new List<Period__c>();
        
        //getting the managed hours record
        List<Managed_Hours__c> lstManagedHours = MH_Utilities.getManagedHours(uniqueKeys);
        System.debug('==uniqueKeys=='+uniqueKeys);
        System.debug('==lstManagedHours=='+lstManagedHours);
        Map<String,RevenueHoursWrapper> mapRevenueWrapper = MH_Utilities.getAggregatedManagedHours(lstManagedHours);
        System.debug('==mapRevenueWrapper=='+mapRevenueWrapper);
        //Looping through the lstPeriod records
        for(Period__c currPeriod : lstPeriod){
            //Creating child object for the period record
            
            Integer startPeriod= Integer.valueOf(currPeriod.Start_Period__c);
            Integer endPeriod = Integer.valueOf(currPeriod.End_Period__c);
            
            for(Integer i = startPeriod; i<=endPeriod ;i++){
                String uniqueKey = MH_Utilities.prepareUniqueKey(currPeriod,i);    
                if(!setDuplicateRecords.contains(uniqueKey)){ //duplicate check
                    RevenueHoursWrapper currWrapper = new RevenueHoursWrapper();
                    Period__c newPeriod = new Period__c();
                    
                    newPeriod.Capability__c = currPeriod.Capability__c;
                    newPeriod.Employee_Capability__c = currPeriod.Employee_Capability__c;
                    newPeriod.Client_Name__c = currPeriod.Client_Name__c;
                    newPeriod.Director__c = currPeriod.Director__c;
                    newPeriod.Employee_Competency_Area__c = currPeriod.Employee_Competency_Area__c;
                    newPeriod.OwnerId = currPeriod.SM_D__c;
                    newPeriod.Request__c = currPeriod.Id;
                    newPeriod.Portfolio__c = currPeriod.Portfolio__c;
                    newPeriod.Market_Offering__c = currPeriod.Market_Offering__c;
                    if(i<10){
                        newPeriod.Start_Period__c = '00'+i;
                        newPeriod.End_Period__c = '00'+i;
                    }
                    else{
                        newPeriod.Start_Period__c = '0'+i;
                        newPeriod.End_Period__c = '0'+i;
                    }
                    newPeriod.Fiscal_Year__c = currPeriod.Fiscal_Year__c;
                    newPeriod.WBS__c = currPeriod.WBS__c;
                    currWrapper = mapRevenueWrapper.get(uniqueKey);
                    if(uniqueKeys.contains(uniqueKey) && currWrapper!=NULL){
                        System.debug('==uniqueKey=='+uniqueKey);
                        System.debug('==currWrapper=='+currWrapper);
                        newPeriod.Total_Client_Hours__c= currWrapper.totalHours ;
                        newPeriod.Total_Net_Revenue__c = currWrapper.totalRevenue;
                        newPeriod.MH_Data_Status__c = 'Data Captured';
                    }
                    else{
                        newPeriod.MH_Data_Status__c = 'Awaiting Data';
                    }
                    newPeriod.RecordTypeId =periodRecordTypeId;
                    lstNewPeriod.add(newPeriod);
                }
            }//end of duplicate check
        } //end of 'for' for adding the period record to a list 
        
        System.debug('==lstNewPeriod.size()=='+lstNewPeriod.size());
        System.debug('==lstNewPeriod=='+lstNewPeriod);  
        if(lstNewPeriod.size()>0){
            try{
                insert lstNewPeriod;
            }
            catch(Exception e){
                System.debug('You are facing this exception - '+e);
            }
        }else{
            System.debug('No new Period data to be added');
        }//end of if for insert
        
    }
    global void finish(Database.BatchableContext bc){
        //Any Finish action to be added
    }
}