public class MH_PeriodTriggerHandler {
    
    public static void beforeInsert(Map<Id,SObject> newItems){
        
    }
    
    public static void afterInsert(Map<Id,SObject> newItems){
        Savepoint sp = Database.setSavepoint();
        try{
            List<String> uniqueKeys = MH_Utilities.prepareUniqueKeys(newItems.values(),'Pending Appproval');
            
            List<Period__c> duplicatePeriods = MH_Utilities.checkDuplicatPeriods(uniqueKeys);
            system.debug('');
            
            if(duplicatePeriods.isEmpty() && !uniqueKeys.isEmpty()){
                
                
                List<Managed_Hours__c> managedHours = MH_Utilities.getManagedHours(uniqueKeys);
                
                Map<String,RevenueHoursWrapper> objectRevHoursWrap = MH_Utilities.getAggregatedManagedHours(managedHours);
                Map<String,List<Managed_Hours__c>> managedHoursMap = MH_Utilities.getManagedHoursToLinkToPeriods(managedHours);
                
                MH_Utilities.InsertPeriods(newItems.values(), objectRevHoursWrap, managedHoursMap);
            }
            else{
                throw new MH_BaseException('Duplicate Rquest(s) found.'); 
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            Trigger.New[0].addError(e.getMessage()); 
        }
    }
    
    public static void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
       for(sObject request : newItems.values()){
           if((string)request.get('Status__c') == 'Rejected'){
               request.put('Consolidated_Client_Hours__c',null);
               request.put('Consolidated_Total_Revenue__c',null);
           }
       }
    }
    
    public static void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            Map<Id, Period__c> newRequestItems = new Map<Id, Period__c>();
            List<Id> newPeriodItems = new List<Id>();
            
            if(!MH_RecursionController.updatePeriodCalled){
                MH_RecursionController.updatePeriodCalled = true;
                
                for(Period__c p : (List<Period__c>)newItems.values()){
                    if(newItems.get(p.Id).get('Status__c') != oldItems.get(p.Id).get('Status__c') 
                       && newItems.get(p.Id).get('Status__c') == 'Pending Approval'){
                           newRequestItems.put(p.Id,p);
                       }
                    else if(newItems.get(p.Id).get('MH_Data_Status__c') != oldItems.get(p.Id).get('MH_Data_Status__c') 
                            && newItems.get(p.Id).get('MH_Data_Status__c') == 'Data Captured'){
                                newPeriodItems.add(p.Id);
                            }
                }
                
                if(!newRequestItems.isEmpty()){
                    
                    List<String> uniqueKeys = MH_Utilities.prepareUniqueKeys(newRequestItems.values(),'Pending Approval');
                    
                    List<Period__c> duplicatePeriods = MH_Utilities.checkDuplicatPeriods(uniqueKeys);
                    
                    if(duplicatePeriods.isEmpty() && !uniqueKeys.isEmpty()){
                        system.debug('uniqueKeys ->' + uniqueKeys.size());
                        List<Managed_Hours__c> managedHours = MH_Utilities.getManagedHours(uniqueKeys);
                        
                        Map<String,RevenueHoursWrapper> objectRevHoursWrap = MH_Utilities.getAggregatedManagedHours(managedHours);
                        Map<String,List<Managed_Hours__c>> managedHoursMap = MH_Utilities.getManagedHoursToLinkToPeriods(managedHours);
                        
                        MH_Utilities.InsertPeriods(newItems.values(), objectRevHoursWrap, managedHoursMap);
                    }
                    else{
                        throw new MH_BaseException('Duplicate Rquest(s) found.'); 
                    }
                }
            }
            if(!newPeriodItems.isEmpty()){
                MH_Utilities.calculateConsolidatedHours(newPeriodItems);
            }
            
            //check for rejected period request
            List<Id> rejectedRequests = New List<Id>();
            for(sObject request : newItems.values()){
                if((string)request.get('Status__c') == 'Rejected'){
					rejectedRequests.add((Id)request.get('Id'));
                }
            }
            
            if(!rejectedRequests.isEmpty()){
                List<Period__c> periods = [select id from Period__c where Request__c IN : rejectedRequests];
                if(!periods.isEmpty()){
                    delete periods;
                }
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            Trigger.New[0].addError(e.getMessage()); 
        }
    }
    
    public static void beforeDelete(Map<Id,SObject> oldItems){
        List<Period__c> periods = [select id from Period__c where Request__c IN : oldItems.keySet()];
        
        system.debug('periods ->' + periods);
        
        if(!periods.isEmpty()){
        	delete periods;
        }
    }
    
}