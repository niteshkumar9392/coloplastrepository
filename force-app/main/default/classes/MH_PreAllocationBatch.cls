/*Author: Vinit Jagwani
*Description: A batch class that would create all the pre-allocation records from Managed Hours, based on - Client,WBS 
& Fiscal Year
*/
global class MH_PreAllocationBatch implements Database.Batchable<sObject>, Database.Stateful{ 
    
    global Map<String, RevenueHoursWrapper> AggregateResult = new Map<String, RevenueHoursWrapper>();
    global DateTime yesterDayDateTime = System.now().addDays(-1);
    global DateTime currDayDateTime = System.now();
    //global String FiscalYear ='2019002';
    global String Anthem ='ANT30375-01';
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT ID,Fiscal_Year_Period__c ,WBS_Level_1__c,LastModifiedDate ,Client_Service_Hours__c,'+
            //    'Client_Name__c,Unique__c,SM_D__c , Profit_Center_Business_Area__c,Employee_Competency_Area__c FROM Managed_Hours__c ';
            'Client_Name__c,SM_D__c  FROM Managed_Hours__c WHERE (LastModifiedDate < :currDayDateTime AND LastModifiedDate  >= :yesterDayDateTime)';
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Managed_Hours__c> lstMangedHours){
        
        Map<String, Id> mapPresentAccount = new Map<String,Id>();
        set<String> setOfWBS = new set<String>();
        map<String,String> mapWBSToClient = new map<String,String>();
        map<Managed_Hours__c,String> mapMHtoUniqueKey = new map<Managed_Hours__c,String>();
        set<String> lstOfNewClientsOrNewWBS = new set<String>();
        Map<String,WBS__c> mapOfWBS = new Map<String,WBS__c>();
        List<String> lstOfTempStringVal = new List<String>();
        Map<String,Pre_Allocation__c> lstOfAllPreAllocation = new Map<String,Pre_Allocation__c>();
        map<String,Double> mapOfPreAllocAndHours = new map<String,Double>();
        map<String,Pre_Allocation__c> mapOfPreAllocAndId = new map<String,Pre_Allocation__c>();
        
        
        for(Managed_Hours__c currMH : lstMangedHours){
            setOfWBS.add(currMH.WBS_Level_1__c);
            String clientName = currMH.Client_Name__c;
            mapWBSToClient.put(currMH.WBS_Level_1__c,clientName.toUpperCase());
            String fiscalYear = currMH.Fiscal_Year_Period__c.substring(0,4);
            mapMHtoUniqueKey.put(currMH,MH_Utilities.createWBSKey(clientName, fiscalYear, currMH.WBS_Level_1__c));
        }
        //fetch all the current Clients
        for(Account acc: MH_Utilities.getAllClients()){
            String clientName = acc.Name;
            if(mapWBSToClient.values().contains(clientName.toUpperCase())){
                mapPresentAccount.put(clientName.toUpperCase(),acc.Id);                      
            }
        }
        //list of all the new clients
        for(String currAcc : mapWBSToClient.values()){
            if(!mapPresentAccount.keySet().contains(currAcc)){
                lstOfNewClientsOrNewWBS.add(currAcc);
            }
        }
        if(lstOfNewClientsOrNewWBS.size() > 0 && lstOfNewClientsOrNewWBS != NULL){
            //System.debug('==New Clients Present==');
            //inserting new clients & creating map that consists of all accounts
            MH_Utilities.createNewClients(lstOfNewClientsOrNewWBS);
            
            for(Account acc: MH_Utilities.getAllClients()){
                if(mapWBSToClient.values().contains(acc.Name)){
                    mapPresentAccount.put(acc.Name,acc.Id);                      
                }
            }
        }
        lstOfNewClientsOrNewWBS.clear();
        //checking for existing WBS and creating new one's
        
        for(WBS__c currWBS : MH_Utilities.getAllWBS()){
            lstOfTempStringVal.add(currWBS.Name);
        }
        for(String currWBS : setOfWBS){
            if(!lstOfTempStringVal.contains(currWBS)){
                lstOfNewClientsOrNewWBS.add(currWBS);
            }
        }
        if(lstOfNewClientsOrNewWBS.size() > 0 && lstOfNewClientsOrNewWBS != NULL){
            //System.debug('==New WBS present==');
            MH_Utilities.createWBS(lstOfNewClientsOrNewWBS,mapWBSToClient,mapPresentAccount);
        }
        for(WBS__c currWBS : MH_Utilities.getAllWBS()){
            mapOfWBS.put(currWBS.Name,currWBS);
        }
        //Creating new pre-allocation records
        
        for(Pre_Allocation__c currPreAlloc :MH_Utilities.getAllPreAllocation()){
            mapOfPreAllocAndHours.put(currPreAlloc.AllocationByWBS__c,currPreAlloc.Client_Hours__c);
            mapOfPreAllocAndId.put(currPreAlloc.AllocationByWBS__c, currPreAlloc);
        }
        Map<String,Pre_Allocation__c> mapNewPreAllocationRecords = new Map<String,Pre_Allocation__c>();
        Double updatedHours = null;
        //checking and updating pre-allocation records if they are present in system.
        for(Managed_Hours__c currMH : mapMHtoUniqueKey.keySet()){
            if(mapOfPreAllocAndId.keySet().contains(mapMHtoUniqueKey.get(currMH))){
                Pre_Allocation__c currPA = mapOfPreAllocAndId.get(mapMHtoUniqueKey.get(currMH));
                Double currPAhours = mapOfPreAllocAndHours.get(mapMHtoUniqueKey.get(currMH));
                updatedHours = currPAhours + currMH.Client_Service_Hours__c;
                currPA.Client_Hours__c = updatedHours;
                mapOfPreAllocAndHours.put(mapMHtoUniqueKey.get(currMH),updatedHours);
                mapOfPreAllocAndId.put(mapMHtoUniqueKey.get(currMH),currPA);
                lstOfAllPreAllocation.put(mapMHtoUniqueKey.get(currMH),currPA);
            }
            else{
                Pre_Allocation__c newPA = MH_Utilities.createPA(currMH,mapPresentAccount,mapOfWBS);
                String newPAUnique = newPA.Client__c+newPA.Fiscal_Year__c+newPA.WBS__c;
                if(mapNewPreAllocationRecords.size() < 1){
                    mapNewPreAllocationRecords.put(newPAUnique,newPA);
                }
                else{
                    if(mapNewPreAllocationRecords.keySet().contains(newPAUnique)){
                        Pre_Allocation__c tempPA = mapNewPreAllocationRecords.get(newPAUnique);
                        tempPA.Client_Hours__c = tempPA.Client_Hours__c+newPA.Client_Hours__c;
                        mapNewPreAllocationRecords.put(newPAUnique,tempPA);
                    }
                    else{
                        mapNewPreAllocationRecords.put(newPAUnique,newPA);
                    }
                }
            }
        }
        try{
            if(lstOfAllPreAllocation!=null && lstOfAllPreAllocation.size()>0){
                update lstOfAllPreAllocation.values();
            }
            if(mapNewPreAllocationRecords!=null && mapNewPreAllocationRecords.size()>0){
                insert mapNewPreAllocationRecords.values();
            }
        }
        catch(Exception e){
            System.debug('The following Error has been recieved: '+e);
        }
        
    }
    global void finish(Database.BatchableContext bc){
        //No logic required for now
    }
}