public class MH_Utilities {
    
    public static List<Pre_Allocation__c> fetchPreAllocationRecords(){
        return [SELECT ID,AllocationByClientHours__c ,AllocationByWBS__c ,Client__c,Client_Hours__c,Fiscal_Year__c,WBS__c From Pre_Allocation__c LIMIT 50000];
    }
    
    public static String createClientKey(String ClientName,String FiscalYear){
        
        String clientKey = ClientName+FiscalYear;
        return clientKey;
    }
    
    public static String createWBSKey(String ClientName,String FiscalYear, String WBSLevel){
        String WBSKey = ClientName+FiscalYear+WBSLevel;
        return WBSKey;
    }
    
    public static List<WBS__c> getAllWBS(){
        List<WBS__c> lstAllWBS = [SELECT ID,Director__c,Client__c,Client_Name__c,Name from WBS__c LIMIT 50000];
        return lstAllWBS;
    }
    
    public static List<Account> getAllClients(){
        return [SELECT Id, Name FROM Account WHERE (RecordType.Name = 'MH Account') LIMIT 50000];
    }
    
    public static List<Pre_Allocation__c> getAllPreAllocation(){
        return [SELECT Id, Name,AllocationByWBS__c,Client_Hours__c FROM Pre_Allocation__c LIMIT 50000];
    }
    
    public static void createNewClients(set<String> clientNames){
        List<Account> newClients = new List<Account>();
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('MH Account').getRecordTypeId();
        System.debug('New Account present');
        for(String clientName : clientNames){
            Account newAcc = new Account();   
            newAcc.Name = clientName;
            newAcc.RecordTypeId = accountRecordTypeId;
            newAcc.Status__c = 'Active';
            newClients.add(newAcc);
        }     
        try{
            insert newClients;
        }
        catch(Exception e){
            System.debug('The following error is recieved'+e);
        }
    }
    
    public static void createWBS(set<String> lstOfNewWBS,map<String,String> mapWBSToClient,Map<String, Id> mapPresentAccount){
        List<WBS__c> newlyCreatedWbs = new List<WBS__c>();
        for(String currStr : lstOfNewWBS){
            String clientName = mapWBSToClient.get(currStr);
            Id clientId = mapPresentAccount.get(clientName);
            WBS__c newWBS = new WBS__c();
            newWBS.Name = currStr ;
            newWBS.Client_Name__c = clientId;
            newWBS.isActive__c = true;
            newlyCreatedWbs.add(newWBS);
        }
        if(newlyCreatedWbs!=null && newlyCreatedWbs.size()>0){
            insert newlyCreatedWbs;
        }
    }
    
    public static Pre_Allocation__c createPA(Managed_Hours__c currMH, Map<String, Id> mapPresentAccount,Map<String,WBS__c>mapOfWBS){
        Pre_Allocation__c newPA = new Pre_Allocation__c();
        newPA.Client__c = mapPresentAccount.get(currMH.Client_Name__c);
        newPa.Client_Hours__c = currMH.Client_Service_Hours__c;
        newPA.WBS__c = mapOfWBS.get(currMH.WBS_Level_1__c).Id;
        String fiscalYear = currMH.Fiscal_Year_Period__c.substring(0,4);
        newPA.Fiscal_Year__c = fiscalYear;
        return newPA;
    }
    
    public static List<String> prepareUniqueKeys(List<Period__c> newItems, String status){
        List<String> uniqueKeys = new List<String>();
        
        for(Period__c objPeriod : newItems){
            
            if( objPeriod.End_Period__c != null && objPeriod.RecordTypeId == Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Request').getRecordTypeId() 
               && objPeriod.Status__c == status){
                   
                   Integer startp = Integer.valueof(objPeriod.Start_Period__c);
                   Integer endP = Integer.valueof(objPeriod.End_Period__c);
                   
                   for(integer i = startp ; i <= endP ; i++){
                       String  str = objPeriod.Fiscal_Year__c; 
                       if(i < 10){
                           str += '00'+ i;
                       }
                       else{
                           str += '0'+ i;
                       }
                       
                       str += objPeriod.Client__c;
                       str += objPeriod.WBS_Text__c;
                       str += objPeriod.Portfolio__c;
                       str += objPeriod.Market_Offering__c;
                       str += objPeriod.Employee_Competency_Area__c;
                       str += objPeriod.Employee_Capability__c;
                       uniqueKeys.add(str);
                   }
               }
        }
        
        return uniqueKeys;
        
    }
    
    public static String prepareUniqueKey(Period__c objPeriod, Integer startPeriod){
        String  str;
        if(objPeriod!=null){
            Integer startp = startPeriod;
            Integer endP = Integer.valueof(objPeriod.End_Period__c);
            
            str = objPeriod.Fiscal_Year__c; 
            if(startp < 10){
                str += '00'+ startp;
            }
            else{
                str += '0'+ startp;
            }
            
            str += objPeriod.Client__c;
            str += objPeriod.WBS_Text__c;
            str += objPeriod.Portfolio__c;
            str += objPeriod.Market_Offering__c;
            str += objPeriod.Employee_Competency_Area__c;
            str += objPeriod.Employee_Capability__c;
        }
        System.debug('==str=='+str);
        return str;
    }
    
    public static List<Period__c> checkDuplicatPeriods(List<String> uniqueKeys){
        
        return [SELECT Name, Client__c,OwnerId,Client_Name__c, WBS_Text__c, Owner.Name, Director__r.Name, Start_Period__c, End_Period__c, Unique_Key__c FROM Period__c 
                WHERE Start_Period__c != null
                AND (RecordType.Name = 'Approved Period' OR RecordType.Name = 'Pending Approval Period' OR RecordType.Name ='Approved')
                AND Unique_Key__c IN : uniqueKeys LIMIT 50000];
    }
    
    public static List<Managed_Hours__c> getManagedHours(List<String> uniqueKeys){
        System.debug('==uniqueKeys=='+uniqueKeys.size());
        System.debug('==count==' + Limits.getQueryRows());
        return[SELECT Unique_Key__c,UniqueText__c,Period__c,Fiscal_Year_Period__c,Client_Service_Hours__c,Net_Revenue__c 
               FROM Managed_Hours__c 
               WHERE Client_Service_Hours__c != null AND UniqueText__c IN : uniqueKeys LIMIT 50000];
    }
    
    public static Map<String,RevenueHoursWrapper> getAggregatedManagedHours(List<Managed_Hours__c> managedHours){
        
        Map<String,RevenueHoursWrapper> objectRevHoursWrap = new Map<String,RevenueHoursWrapper>();
        
        for(Managed_Hours__c managedhr : managedHours){
            
            if(objectRevHoursWrap.containsKey(managedhr.Unique_Key__c)){
                objectRevHoursWrap.get(managedhr.Unique_Key__c).totalRevenue += managedhr.Net_Revenue__c ;   
                objectRevHoursWrap.get(managedhr.Unique_Key__c).totalHours += managedhr.Client_Service_Hours__c;
            }
            else{
                RevenueHoursWrapper wrap = new RevenueHoursWrapper();
                wrap.totalRevenue=managedhr.Net_Revenue__c ;
                wrap.totalHours=managedhr.Client_Service_Hours__c;
                objectRevHoursWrap.put(managedhr.Unique_Key__c, wrap);
            }
        }
        
        return objectRevHoursWrap;
        
    }
    public static Map<String,List<Managed_Hours__c>> getManagedHoursToLinkToPeriods(List<Managed_Hours__c> managedHours){
        
        Map<String,List<Managed_Hours__c>> managedHoursMap = new Map<String,List<Managed_Hours__c>>();    
        
        for(Managed_Hours__c managedhr : managedHours){ 
            if(managedHoursMap.containsKey(managedhr.Unique_Key__c)){
                managedHoursMap.get(managedhr.Unique_Key__c).add(managedhr);
            }
            else{
                managedHoursMap.put(managedhr.Unique_Key__c, new List<Managed_Hours__c>{managedhr});
            }
        }   
        
        return managedHoursMap;
    }
    
    public static void InsertPeriods(List<Period__c> newItems, Map<String,RevenueHoursWrapper> objectRevHoursWrap, Map<String,List<Managed_Hours__c>> managedHoursMap){
        
        system.debug('Inside');
        
        List<Period__c> lstNewPeriods = new List<Period__c>();
        List<Managed_Hours__c> managedHoursToUpdate = new List<Managed_Hours__c>();
        
        for(Period__c objPeriod : newItems){
            
            Integer startp = Integer.valueof(objPeriod.Start_Period__c);
            Integer endP = Integer.valueof(objPeriod.End_Period__c);
            
            if(objPeriod.RecordTypeId == Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Request').getRecordTypeId() && objPeriod.Status__c == 'Pending Approval'){
                
                for(integer i = startp ; i <= endP ; i++){
                    String  str = objPeriod.Fiscal_Year__c; 
                    if(i < 10){
                        str += '00'+ i;
                    }
                    else{
                        str += '0'+ i;
                    }
                    str += objPeriod.Client__c;
                    str += objPeriod.WBS_Text__c;
                    str += objPeriod.Portfolio__c;
                    str += objPeriod.Market_Offering__c;
                    str += objPeriod.Employee_Competency_Area__c;
                    str += objPeriod.Employee_Capability__c;
                    
                    
                    Period__c period = new Period__c();
                    
                    period.Request__c = objPeriod.Id;
                    period.RecordTypeId = Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Approved Period').getRecordTypeId();
                    period.WBS__c = objPeriod.WBS__c;
                    period.Capability__c = objPeriod.Capability__c;
                    if(i < 10){
                        period.Start_Period__c = '00'+ i;
                    }
                    else{
                        period.Start_Period__c = '0'+ i;
                    }
                    
                    period.End_Period__c = period.Start_Period__c;
                    period.Fiscal_Year__c = objPeriod.Fiscal_Year__c;
                    period.Director__c = objPeriod.Director__c;
                    period.Employee_Competency_Area__c = objPeriod.Employee_Competency_Area__c;
                    
                    period.Market_Offering__c = objPeriod.Market_Offering__c;
                    period.Portfolio__c = objPeriod.Portfolio__c;
                    period.Employee_Capability__c = objPeriod.Employee_Capability__c;
                    period.Client_Name__c = objPeriod.Client_Name__c;
                    
                    if(objectRevHoursWrap.get(str) != null){
                        
                        period.Total_Client_Hours__c = objectRevHoursWrap.get(str).totalHours;
                        period.Total_Net_Revenue__c = objectRevHoursWrap.get(str).totalRevenue;
                        period.MH_Data_Status__c = 'Data Captured';
                        
                    }
                    else{
                        period.MH_Data_Status__c = 'Awaiting Data';
                    }
                    
                    lstNewPeriods.add(period);
                }
            }
        }
        
        if(!lstNewPeriods.isEmpty()){
            insert lstNewPeriods;
            
            List<Id> newIds = new List<Id>(); 
            List<Id> parentIds = new List<Id>();
            List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest> ();
            
            for(Period__c objPeriod : lstNewPeriods){
                newIds.add(objPeriod.Id);
                parentIds.add(objPeriod.Request__c);
            }
            
            for(Period__c objPeriod : [SELECT Id, Director__r.Name, OwnerId,Request__r.Ownerid,Client_Name__c,RecordTypeId,MH_Data_Status__c,Unique_Key__c 
                                       FROM Period__c 
                                       WHERE Id IN :newIds 
                                       AND RecordTypeId = :Schema.SObjectType.Period__c.getRecordTypeInfosByName().get('Approved Period').getRecordTypeId()
                                       AND MH_Data_Status__c = 'Data Captured' LIMIT 50000]){
                                           
                                           if(managedHoursMap.get(objPeriod.Unique_Key__c) != null){
                                               for(Managed_Hours__c mh : managedHoursMap.get(objPeriod.Unique_Key__c)){
                                                   mh.Period__c = objPeriod.Id;
                                                   mh.OwnerId = objPeriod.OwnerId;
                                                   mh.Client__c = objPeriod.Client_Name__c;
                                                   mh.SM_D__c  = objPeriod.Director__r.Name;
                                               }
                                               managedHoursToUpdate.addAll(managedHoursMap.get(objPeriod.Unique_Key__c));
                                           }
                                       }
            
            if(!managedHoursToUpdate.isEmpty()){
                update managedHoursToUpdate;
                calculateConsolidatedHours(parentIds);
            }
        }
    }
    
    public static void calculateConsolidatedHours(List<String> periodIds){
        List<Period__c> lstRequestPeriods = [SELECT Consolidated_Client_Hours__c,
                                             Consolidated_Total_Revenue__c,
                                             (SELECT Total_Client_Hours__c,Total_Net_Revenue__c 
                                              FROM ChildPeriods__r 
                                              WHERE MH_Data_Status__c = 'Data Captured' 
                                              AND (Total_Client_Hours__c != null 
                                                   OR Total_Net_Revenue__c != null)) 
                                             FROM Period__c 
                                             WHERE ID IN:periodIds LIMIT 50000];
        
        for(Period__c pd : lstRequestPeriods){
            Decimal totalHrs = 0;
            Decimal totalRevenue = 0;
            
            for(Period__c innerPD : pd.ChildPeriods__r){
                if(innerPD.Total_Client_Hours__c != null){
                    totalHrs += innerPD.Total_Client_Hours__c;
                }
                if(innerPD.Total_Net_Revenue__c != null){
                    totalRevenue += innerPd.Total_Net_Revenue__c;
                }
            }
            pd.Consolidated_Client_Hours__c = totalHrs;
            pd.Consolidated_Total_Revenue__c = totalRevenue;
        }
        
        if(!lstRequestPeriods.isEmpty()){
            update lstRequestPeriods; 
        }
    }    
}