public class MH_WBSTriggerHandler {
    
    public static void beforeInsert(List<WBS__c> newItems){
        Savepoint sp = Database.setSavepoint();
        try{
            Set<String> setWBSClientName = new Set<String>();
            Map<WBS__c,String> mapWBStoClientName = new Map<WBS__c,String>();
            List<WBS__c> listWBS = new List<WBS__c>();
            for(WBS__c currWBS : newItems){
                if(currWBS.Client__c != Null && currWBS.Client__c !='' && currWBS.Client_Name__c == NULL){
                    listWBS.add(currWBS);
                    setWBSClientName.add(currWBS.Client__c);
                    mapWBStoClientName.put(currWBS, currWBS.Client__c);
                }
            }
            Map<String,Id> mapAccountNamesAndId = new Map<String,Id>();
            for(Account acc:[SELECT ID,NAME FROM ACCOUNT WHERE RECORDTYPE.NAME='MH Account' AND NAME IN :setWBSClientName]){
                mapAccountNamesAndId.put(acc.Name, acc.Id);
            }
            for(WBS__c currWBS : listWBS){
                currWBS.Client_Name__c = mapAccountNamesAndId.get(mapWBStoClientName.get(currWBS));
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            Trigger.New[0].addError(e.getMessage()); 
        }
    }
    
}