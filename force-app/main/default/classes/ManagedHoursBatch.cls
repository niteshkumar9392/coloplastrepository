global class ManagedHoursBatch implements Database.Batchable<sObject>{
    
    global String query;
    
    global void setQuery(String q){
         query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Managed_Hours__c> managedHours){
       
        Map<String,List<Managed_Hours__c>> wbcPeriod = new Map<String,List<Managed_Hours__c>>();
        
        Set<String> wbsCodes = new Set<String>();
        
        for(WBS__c wb : [SELECT Name from WBS__c]){
            wbsCodes.add(wb.Name);
        }
        
        List<WBS__c> newWbs = new List<WBS__c>();
        
        for(Managed_Hours__c mh : managedHours){
            if(!wbsCodes.contains(mh.WBS_Level_1__c)){
                newWbs.add(new WBS__c(Name = mh.WBS_Level_1__c,WBS_Level_1_Text__c = mh.WBS_Level_1_Text__c));
            }
            
            if(wbcPeriod.containsKey(mh.WBS_Level_1__c+'='+mh.Fiscal_Year_Period__c)){
                wbcPeriod.get(mh.WBS_Level_1__c+'='+mh.Fiscal_Year_Period__c).add(mh);
            }
            else{
                wbcPeriod.put(mh.WBS_Level_1__c+'='+mh.Fiscal_Year_Period__c, new List<Managed_Hours__c>{mh});
            }
        }
        
        if(!newWbs.isEmpty()){
            insert newWbs;    
        }
        
        Map<String,Id> wbsMap = new Map<String, Id>();
        for(WBS__c wbs : [select Id, Name from WBS__c]){
            wbsMap.put(wbs.Name, wbs.Id);
        }
        
        List<Period__c> periods = new List<Period__c>();
        for(String str : wbcPeriod.keySet()){
            
            Decimal netServiceRevenue = 0;
            Decimal netClientService = 0;
            String key = str.split('=')[0];
            String key2 = str.split('=')[1];
            
            system.debug(str+ ' ' + key + ' ' + key2.substring(0,4) + ' ' + key2.substring(4,7));
            
            for(Managed_Hours__c mh : wbcPeriod.get(str)){
                netServiceRevenue += mh.Net_Service_Revenue__c;
                netClientService +=  mh.Client_Service_Hours__c;
            } 
            
            Period__c period = new Period__c(Total_Client_Hours__c=netClientService,
                                             Total_Net_Revenue__c=netServiceRevenue,
                                             WBS__c = wbsMap.get(key),
                                             Fiscal_Year__c = key2.substring(0,4),
                                             Start_Period__c = key2.substring(4,7));
            periods.add(period);
        }
        
        if(!periods.isEmpty()){
            insert periods;
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}