public class MyTasksController {
    
    @AuraEnabled
    public static Map<String,List<RequestWrapper>> getRequests(){
        
        Map<String,List<RequestWrapper>> requestMap = new Map<String,List<RequestWrapper>>();
 
        for(Period__c period : [Select Name, Director__c,WBS__c,WBS__r.Name, Start_Period__c, Client_Name__c, Client_Name__r.Name, End_Period__c, Fiscal_Year__c, Client_Text__c, 
                                WBS_TextValue__c, Portfolio__c,Requested_by__c , Market_Offering__c, Employee_Competency_Area__c, 
                                Employee_Capability__c 
                                FROM Period__c 
                                WHERE RecordType.Name = 'Request'
                                AND Director__c = :UserInfo.getUserId()
        						AND (Status__c = 'Pending Approval')
                                ORDER BY Start_Period__c ASC LIMIT 100]){
                                    
                                    String key = ''; 
                                    key += 'Period : '+ period.Start_Period__c;
                                    key += ' - ' + period.Client_Name__r.Name;
                                    key += ' - ' + period.WBS__r.Name; 
                                    key += ' - ' + period.Fiscal_Year__c;
                                    
                                    RequestWrapper reqWrapper = new RequestWrapper();
                                    reqWrapper.clientId = period.Client_Name__c;
                                    reqWrapper.director = period.Director__c;
                                    reqWrapper.requestedBy = period.Requested_by__c;
                                    reqWrapper.clientName = period.Client_Name__r.Name;
                                    reqWrapper.WBS = period.WBS__r.Name;
                                    reqWrapper.WBSId = period.WBS__C;
                                    reqWrapper.fiscalYear = period.Fiscal_Year__c;
                                    reqWrapper.period = period.Start_Period__c;
                                    
                                    /*if(requestMap.containsKey(key)){
                                        boolean flag = false;
                                        for(RequestWrapper wrap : requestMap.get(key)){
                                           if(wrap.WBSId == reqWrapper.WBSId
                                              && wrap.fiscalYear == wrap.fiscalYear
                                              && wrap.period == wrap.period){
                                                  flag = true;
                                                  break;
                                              }
                                        }
                                        
                                        if(flag == false){
                                            requestMap.get(key).add(reqWrapper); 
                                        }
                                       	
                                    }
                                    else{*/
                                        requestMap.put(key,new List<RequestWrapper>{reqWrapper});
                                   // }  
                                }    
        return requestMap;
    }
}