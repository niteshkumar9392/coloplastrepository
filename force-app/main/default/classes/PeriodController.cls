public with sharing class PeriodController {
    
    @AuraEnabled
    public static void submitRequests(List<Period__c> listRequest){
        insert listRequest;
    }
    
    @AuraEnabled
    public static String getUserProfile(){
        return [Select Profile.Name from User where Id = :UserInfo.getUserId()].Profile.Name;
    }
    
    @AuraEnabled
    public static List<Period__c> checkDuplicatePeriods(Period__c objPeriod){
        
        system.debug(JSON.serializePretty(objPeriod));
        
        List<String> uniqueKeys = new List<String>();
        
        Integer startp = Integer.valueof(objPeriod.Start_Period__c);
        Integer endP = Integer.valueof(objPeriod.End_Period__c);
        
        for(integer i = startp ; i <= endP ; i++){
            String  str = objPeriod.Fiscal_Year__c; 
            if(i < 10){
                str += '00'+ i;
            }
            else{
                str += '0'+ i;
            }
            
            str += objPeriod.Client_Text__c;
            str += objPeriod.WBS_TextValue__c;
            str += objPeriod.Portfolio__c;
            str += objPeriod.Market_Offering__c;
            str += objPeriod.Employee_Competency_Area__c;
            str += objPeriod.Employee_Capability__c;
            uniqueKeys.add(str);
        }
        
        system.debug('uniqueKeys -> ' + uniqueKeys);
        
        return [SELECT Name, Client__c, WBS_Text__c, Owner.Name, Director__r.Name, Start_Period__c, End_Period__c FROM Period__c 
                WHERE Start_Period__c != null
                AND (RecordType.Name = 'Approved Period' OR RecordType.Name = 'Pending Approval Period')
                AND Unique_Key__c IN : uniqueKeys];
    }
    
    @AuraEnabled
    public static List<Account> searchClients(String searchText){
        string tempInput = '%' + searchText + '%';
        return [Select Id, Name, (Select Name from WBS__r) from Account WHERE Name LIKE :tempInput];
    }
    
    @AuraEnabled
    public static List<User> searchDirectors(String searchText){
        string tempInput = '%' + searchText + '%';
        return [Select Id, Name, Profile.Name from User WHERE Name LIKE :tempInput and Profile.Name = 'Director'];
    }
    
    @AuraEnabled
    public static PeriodPicklistWrapper getPeriodPicklistValues(){
        
        List<SelectOption> lstEmployeeCapability = PeriodController.getPicklistValuesUtility('Period__c','Employee_Capability__c');
        List<SelectOption> lstEmployeeCompetencyArea = PeriodController.getPicklistValuesUtility('Period__c','Employee_Competency_Area__c');
        List<SelectOption> lstEndPeriod = PeriodController.getPicklistValuesUtility('Period__c','End_Period__c');
        List<SelectOption> lstlstFiscalYear = PeriodController.getPicklistValuesUtility('Period__c','Fiscal_Year__c');
        List<SelectOption> lstPortfolio = PeriodController.getPicklistValuesUtility('Period__c','Portfolio__c');
        List<SelectOption> lstMarketOffering = PeriodController.getPicklistValuesUtility('Period__c','Market_Offering__c');
        List<SelectOption> lstStartPeriod = PeriodController.getPicklistValuesUtility('Period__c','Start_Period__c');  
        
        PeriodPicklistWrapper wrapper = new PeriodPicklistWrapper();
        wrapper.lstEmployeeCapability = lstEmployeeCapability;
        wrapper.lstEmployeeCompetencyArea = lstEmployeeCompetencyArea;
        wrapper.lstEndPeriod = lstEndPeriod;
        wrapper.lstlstFiscalYear = lstlstFiscalYear;
        wrapper.lstPortfolio = lstPortfolio;
        wrapper.lstMarketOffering = lstMarketOffering;
        wrapper.lstStartPeriod = lstStartPeriod;
        return wrapper;
        
    }
    
    public static List<SelectOption> getPicklistValuesUtility(String objectAPIName, String fieldAPIName){
        
        List<SelectOption> lstPicklistValues = new List<SelectOption>();
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectAPIName);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldAPIName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        lstPicklistValues.add(new SelectOption('--None--',null));
        
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPicklistValues.add(new SelectOption(a.getLabel(),a.getValue()));//add the value  to our final list
        }
        
        return lstPicklistValues;
    }
}