public class PeriodPicklistWrapper {
    
    @AuraEnabled
    public List<SelectOption> lstlstFiscalYear;
    
    @AuraEnabled
    public List<SelectOption> lstPortfolio;
    
    @AuraEnabled
    public List<SelectOption> lstEmployeeCompetencyArea;
    
    @AuraEnabled
    public List<SelectOption> lstMarketOffering;
    
    @AuraEnabled
    public List<SelectOption> lstEmployeeCapability;
    
    @AuraEnabled
    public List<SelectOption> lstStartPeriod;
    
    @AuraEnabled
    public List<SelectOption> lstEndPeriod;
}