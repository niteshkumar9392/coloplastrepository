public class RequestWrapper {
	
    @AuraEnabled
    public String clientId;
    
    @AuraEnabled
    public String director;
    
    @AuraEnabled
    public String requestedBy;
    
    @AuraEnabled 
    public String clientName;
    
    @AuraEnabled 
    public String WBS;
    
    @AuraEnabled 
    public String WBSId;
    
    @AuraEnabled 
    public String fiscalYear;
    
    @AuraEnabled 
    public String period;
}