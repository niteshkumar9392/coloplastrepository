public with sharing class SearchClientsController {
	
    @AuraEnabled
    public static AllocationWrapper searchPreAllocatedHours(String clientId, String fiscalYear){
        List<Pre_Allocation__c > preAllocations = [ SELECT Client__c, Client__r.Name , Client_Hours__c , Fiscal_Year__c , WBS__c, WBS__r.Name
                                                    FROM Pre_Allocation__c 
                                                    WHERE Client__c = :clientId AND Fiscal_Year__c = :fiscalYear];
        
        List<Allocated_Hours__c > alloted_hours = [ SELECT Client__c, Client__r.Name , Client_Hours__c , Fiscal_Year__c , WBS__c, WBS__r.Name,
                                                    Senior_Manager__c, Senior_Manager__r.Name, Start_Period__c, End_Period__c 
                                                    FROM Allocated_Hours__c 
                                                    WHERE Client__c = :clientId AND Fiscal_Year__c = :fiscalYear];
        
        return new AllocationWrapper(alloted_hours,preAllocations);
        
    }
    
    @AuraEnabled
    public static List<Account> searchClients(String searchText){
        string tempInput = '%' + searchText + '%';
        return [Select Id, Name, (Select Name from WBS__r) from Account WHERE Name LIKE :tempInput];
    }
    
    @AuraEnabled
    public static Map<String, Decimal> getPeriodWiseData(String clientName, String fiscalYear){
        
        Map<String, Decimal> data = new Map<String, Decimal>();
        fiscalYear = fiscalYear + '%';
        for(Managed_Hours__c mh : [SELECT Client_Name__c, Fiscal_Year_Period__c, Client_Service_Hours__c FROM Managed_Hours__c where Client_Name__c = :clientName and Fiscal_Year_Period__c LIKE :fiscalYear LIMIT 50000]){
           
            Decimal hours = 0;  
            if(data.containsKey(mh.Fiscal_Year_Period__c.subString(4,7))){
                Decimal temp = data.get(mh.Fiscal_Year_Period__c.subString(4,7));
                temp += mh.Client_Service_Hours__c;
                data.put(mh.Fiscal_Year_Period__c.subString(4,7),temp);
            }
            else{
                data.put(mh.Fiscal_Year_Period__c.subString(4,7), mh.Client_Service_Hours__c);
            }
        }

		return data;
    }
}