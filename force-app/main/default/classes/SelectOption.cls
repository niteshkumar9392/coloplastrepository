public class SelectOption {
    
    @AuraEnabled
    public string label;
    
    @AuraEnabled
    public String value;
    
    public SelectOption(String label, String value){
        this.label = label;
        this.value = value;
    }

}